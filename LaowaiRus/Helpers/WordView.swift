//
//  WordView.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 17.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class WordView: UIImageView {
    
    private struct Constants {
        static let bgLineColor: UIColor = .yellow
        static let bgBorderLineWidth: CGFloat = 3
        static let bgDashLineWidth: CGFloat = 1
    }
    
    var strokeLabelAppend = false
    
    var word: StrokeOrder? = nil
    
    var strokeIndex: Int = -1
    
    var isPause = false
    var isPlay = false
    var indexChanged = false
    
    
    var strokeLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: 50, height: 30))
      label.text = "0/"
      label.textColor = UIColor.white
      return label
    }()
    
    lazy var bgImage: UIImage = createBgImage(self.bounds.size)
    public lazy var animation:CABasicAnimation = createAnimation()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.image = bgImage
        self.contentMode = .center
    }
    
    public func drawWord(_ word: StrokeOrder) -> Void {
        self.word = word
        
        drawOrganic()
        
        strokeIndex = 0
        drawStroke()
    }
    
    public func publicDrawOrganic() -> Void {
        let organicLayer = CAShapeLayer()
        organicLayer.fillColor = UIColor.clear.cgColor
        organicLayer.path = word!.outline.cgPath
        organicLayer.setAffineTransform(CGAffineTransform.init(scaleX: frame.width/1024, y: -frame.height/1024).translatedBy(x: 0, y: -900))
        layer.addSublayer(organicLayer)
    }
    
    private func drawOrganic() -> Void {
        let organicLayer = CAShapeLayer()
        organicLayer.fillColor = UIColor.clear.cgColor
        organicLayer.path = word!.outline.cgPath
        organicLayer.setAffineTransform(CGAffineTransform.init(scaleX: frame.width/1024, y: -frame.height/1024).translatedBy(x: 0, y: -900))
        layer.addSublayer(organicLayer)
    }
    
        
    public func pauseAnimation(){
        isPlay = false
        self.layer.removeAllAnimations()
        clearImage()
        self.drawAllImage(word!)
    }
    
    public func continueAnimation(){
        isPlay = true
        if strokeIndex < word!.strokes.count - 1 {
            strokeIndex = strokeIndex + 1
            drawStroke()
        }
    }
    
    
    public func nextIndex() {
        pauseAnimation()

        print(strokeIndex)
        if strokeIndex == -1 {
            
            strokeIndex = 0
            
            drawStroke()
            
        }else{
                
            if strokeIndex < word!.strokes.count - 1{

                    strokeIndex = strokeIndex + 1
                    drawStroke()
                   
            }
        }
    }
    
    public func clearImage(){
        //pauseAnimation()
        layer.sublayers = nil
    }
    
    public func prevIndex() {
        pauseAnimation()
        if strokeIndex >= 0{
            layer.sublayers?.last?.removeFromSuperlayer()
            strokeIndex = strokeIndex - 1
            
            strokeLabel.text = "\(strokeIndex+1)/" + String(word!.strokes.count)
                //drawStroke()
        }/*else if strokeIndex == 0{
            layer.sublayers?.last?.removeFromSuperlayer()
            
            strokeLabel.text = "0/" + String(word!.strokes.count)
        }*/
    }
    
    public func drawAllImageForBG(_ word: StrokeOrder) -> Void{
        
        
        self.word = word
            
        drawOrganic()
        strokeIndex = word.strokes.count-1
        
        for item in word.strokes{
            let stroke = item
            let strokeOutline = stroke.outline
            let strokeMedian = stroke.median
            
            
            let clipLayer = CAShapeLayer()
            clipLayer.fillColor = UIColor.gray.cgColor
            clipLayer.path = strokeOutline.cgPath
            
            let drawLayer = CAShapeLayer()
            drawLayer.strokeColor = UIColor.gray.cgColor
            drawLayer.fillColor = UIColor.clear.cgColor
            drawLayer.path = strokeMedian.cgPath
            drawLayer.lineWidth = 128
            drawLayer.lineJoin = CAShapeLayerLineJoin.round
            drawLayer.lineCap = CAShapeLayerLineCap.round
            drawLayer.setAffineTransform(CGAffineTransform.init(scaleX: frame.width/1024, y: -frame.height/1024).translatedBy(x: 0, y: -900))
            //drawLayer.add(animation, forKey: "path")
            drawLayer.mask = clipLayer
            layer.addSublayer(drawLayer)
        }
    }
    
    public func drawAllImage(_ word: StrokeOrder) -> Void{
        
        
        self.word = word
            
        drawOrganic()
        strokeIndex = word.strokes.count-1
        
        for item in word.strokes{
            let stroke = item
            let strokeOutline = stroke.outline
            let strokeMedian = stroke.median
            
            
            let clipLayer = CAShapeLayer()
            clipLayer.fillColor = UIColor.gray.cgColor
            clipLayer.path = strokeOutline.cgPath
            
            let drawLayer = CAShapeLayer()
            drawLayer.strokeColor = UIColor.strokeColor.cgColor
            drawLayer.fillColor = UIColor.clear.cgColor
            drawLayer.path = strokeMedian.cgPath
            drawLayer.lineWidth = 128
            drawLayer.lineJoin = CAShapeLayerLineJoin.round
            drawLayer.lineCap = CAShapeLayerLineCap.round
            drawLayer.setAffineTransform(CGAffineTransform.init(scaleX: frame.width/1024, y: -frame.height/1024).translatedBy(x: 0, y: -900))
            //drawLayer.add(animation, forKey: "path")
            drawLayer.mask = clipLayer
            layer.addSublayer(drawLayer)
            
        }
    }
    
    private func drawStroke() -> Void {
        
        let stroke = word!.strokes[strokeIndex]
        let strokeOutline = stroke.outline
        let strokeMedian = stroke.median
        
        
        let clipLayer = CAShapeLayer()
        clipLayer.fillColor = UIColor.gray.cgColor
        clipLayer.path = strokeOutline.cgPath
        
        let drawLayer = CAShapeLayer()
        drawLayer.strokeColor = UIColor.strokeColor.cgColor
        drawLayer.fillColor = UIColor.clear.cgColor
        drawLayer.path = strokeMedian.cgPath
        drawLayer.lineWidth = 128
        drawLayer.lineJoin = CAShapeLayerLineJoin.round
        drawLayer.lineCap = CAShapeLayerLineCap.round
        drawLayer.setAffineTransform(CGAffineTransform.init(scaleX: frame.width/1024, y: -frame.height/1024).translatedBy(x: 0, y: -900))
        drawLayer.add(self.animation, forKey: "path")
        drawLayer.mask = clipLayer
        layer.addSublayer(drawLayer)
    }
}

extension WordView: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if isPlay{
            if strokeIndex < word!.strokes.count-1 {
                strokeIndex = strokeIndex + 1
                drawStroke()
            }
        }
    }
}

extension WordView {
    
    func createAnimation() -> CABasicAnimation {
        let animation = CABasicAnimation.init(keyPath: "strokeEnd")
        animation.duration = 1.0
        animation.fromValue = 0
        animation.toValue = 1.0
        animation.delegate = self
        return animation
    }
    
    func createBgImage(_ size: CGSize) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()!
        
        let randomColor = UIColor.gray
        
        context.setStrokeColor(randomColor.cgColor)
        
        let dashPath = UIBezierPath()
        dashPath.move(to: CGPoint(x: 0, y: 0))
        dashPath.addLine(to: CGPoint(x: size.width, y: size.height))
        dashPath.move(to: CGPoint(x: size.width, y: 0))
        dashPath.addLine(to: CGPoint(x:0, y: size.height))
        dashPath.move(to: CGPoint(x: 0, y: size.height/2))
        dashPath.addLine(to: CGPoint(x: size.width, y: size.height/2))
        dashPath.move(to: CGPoint(x: size.width/2, y: 0))
        dashPath.addLine(to: CGPoint(x: size.width/2, y: size.height))
        context.saveGState()
        context.setLineDash(phase: 3, lengths: [3])
        dashPath.lineWidth = Constants.bgDashLineWidth
        dashPath.stroke()
        
        
        context.restoreGState()
        
        let borderPath = UIBezierPath.init(rect: CGRect(x: Constants.bgBorderLineWidth / 2,
                                                        y: Constants.bgBorderLineWidth/2,
                                                        width: size.width-Constants.bgBorderLineWidth,
                                                        height: size.height-Constants.bgBorderLineWidth));
        borderPath.lineWidth = Constants.bgBorderLineWidth
        borderPath.stroke()
        
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}
