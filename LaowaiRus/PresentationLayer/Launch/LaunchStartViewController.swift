//
//  LaunchStartViewController.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 21.01.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit
import SwiftyGif

class LaunchStartViewController: UIViewController {

    let logoAnimationView = LogoAnimationView()
    var SQLServ = SQLService()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(logoAnimationView)
        logoAnimationView.pinEdgesToSuperView()
        logoAnimationView.logoGifImageView.delegate = self// as? SwiftyGifDelegate
        self.SQLServ.initTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        logoAnimationView.logoGifImageView.startAnimatingGif()
    }

    func loadingEnd(){
        
        if UserDefaults.standard.object(forKey: "isFirstOpen") != nil{
            performSegue(withIdentifier: "next", sender: self)
        }else{
            UserDefaults.standard.set(true, forKey: "isFirstOpen")
            showStartedScreen()
        }
    }
    
    func showStartedScreen(){
        let vc = StarterViewController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }

}

extension LaunchStartViewController: SwiftyGifDelegate {
    func gifDidStop(sender: UIImageView) {
        logoAnimationView.isHidden = true
        loadingEnd()
    }
}
    
    

