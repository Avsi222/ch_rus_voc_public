//
//  FavoriteListViewController.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 21.01.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class FavoriteListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var favoriteList = [FavoriteList]()
    var selectedList: FavoriteList!
    
    var HSKListsNames = [
        "HSK1",
        "HSK2",
        "HSK3",
        "HSK4",
        "HSK5",
        "HSK6"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Списки"
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "FavoriteListTVC", bundle: nil), forCellReuseIdentifier: "cell")
        
        loadFavoriteList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadFavoriteList()
    }
    
    func loadFavoriteList(){
        if let favLists = FavoriteService().getListFav(){
            favoriteList = favLists
            //добавляем историю поиска
            let searchHistory = FavoriteList(id: favoriteList.count, name: Titles.historyListName)
            favoriteList.append(searchHistory)
            let piniynFavs = FavoriteList(id: favoriteList.count, name: Titles.piniynListName)
            favoriteList.append(piniynFavs)
            tableView.reloadData()
        }
    }
    
    func openHSKFavorites(_ hskListId: Int){
        let vc = UIStoryboard.init(name: "Favorite", bundle: Bundle.main).instantiateViewController(withIdentifier: "FavoriteViewController") as? FavoriteViewController
        
        vc?.isHskList = true
        vc?.hskListId = hskListId + 1
        vc?.isEditable = false
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if selectedList != nil {
            if segue.destination is FavoriteViewController{
                let vc = segue.destination as! FavoriteViewController
                vc.favorites = selectedList
                if selectedList.name == Titles.historyListName{
                    vc.isRecents = true
                }else{
                    if selectedList.name == Titles.piniynListName{
                        vc.isPiniyns = true
                    }else{
                        vc.isRecents = false
                    }
                }
            }
        }
        
    }
    
}
extension FavoriteListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return favoriteList.count
        }else{
            return HSKListsNames.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FavoriteListTVC
        if indexPath.section == 0{
            let currentFavList = favoriteList[indexPath.row]
            
            cell.initCell(currentFavList)
        }else{
            cell.listNameLabel.text = HSKListsNames[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            selectedList = favoriteList[indexPath.row]
            performSegue(withIdentifier: "openList", sender: nil)
        }else{
            openHSKFavorites(indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if indexPath.section == 1{
            return nil
        }
        if indexPath.row > favoriteList.count - 2{
            print("It RECENETS STUPIDO CACTUS")
            return nil
        }else{

            let deleteAction = UITableViewRowAction(style: .destructive, title: Titles.rowActionDelete) { (action, indexPathAction) in
                let listId = self.favoriteList[indexPath.row].id
                FavoriteService().removeList(listId: listId)
                self.loadFavoriteList()
            }
            
            let updateAction = UITableViewRowAction(style: .normal, title: Titles.rowActionChange) { (action, indexPathAction) in
                let listId = self.favoriteList[indexPath.row].id
                self.askNewNameList(idList: listId)
                self.loadFavoriteList()
            }
            
            return [deleteAction, updateAction]
        }
    }
    
    func askNewNameList(idList: Int){
        let alert = UIAlertController(title: Titles.titleAlertChangeNameList, message: Titles.messageAlertChangeNameList, preferredStyle: .alert)
        
        var nameTextField = UITextField()
        
        alert.addTextField { (result) in
            nameTextField = result
        }
        
        let closeAction = UIAlertAction(title: Titles.alertCancelAction, style: .cancel, handler: nil)
        let saveAction = UIAlertAction(title: Titles.saveAlertChangeNameList, style: .default) { (action) in
            let newNameList = nameTextField.text ?? ""
            FavoriteService().editList(newName: newNameList, listId: idList)
            self.loadFavoriteList()
        }
        alert.addAction(closeAction)
        alert.addAction(saveAction)
        
        self.present(alert, animated: true)
    }
}
