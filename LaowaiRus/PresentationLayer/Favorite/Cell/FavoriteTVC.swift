//
//  FavoriteTVC.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 02/02/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class FavoriteTVC: UITableViewCell {
    
    @IBOutlet weak var wordLabel: UILabel!
    @IBOutlet weak var translateLabel: UILabel!
    
    func initCell(_ word: Word){
        wordLabel.text = word.word
        translateLabel.attributedText = word.combination
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
