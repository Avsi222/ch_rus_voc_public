//
//  FavoriteListTaVC.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 06.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class FavoriteListTVC: UITableViewCell {

   @IBOutlet weak var listNameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        listNameLabel.textColor = .wordTextCellColor
        listNameLabel.font = UIFont.favoriteListCellLabelFont
        self.backgroundColor = .favoriteListBGColor
    }

    func initCell(_ favList: FavoriteList){
        listNameLabel.text = favList.name
    }
    
}
