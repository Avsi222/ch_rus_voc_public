//
//  FavoriteViewController.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 21.01.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class FavoriteViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var favorites: FavoriteList!
    var favoriteWords = [Word]()
    var isEditable: Bool = true
    var isRecents: Bool = false
    var isHskList: Bool = false
    var hskListId: Int = 1
    var currentPage: Int = 1
    
    var piniynWord = [String]()
    
    var isPiniyns: Bool = false
    
    var isLoaded: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isHskList == true {
            self.navigationItem.title = "HSK \(hskListId)"
        }
        else{
            self.navigationItem.title = favorites.name
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "WordTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        loadFavorite()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //loadFavorite()
    }
    
    private func loadFavorite(){
        if isRecents{
            currentPage = 0
            loadRecentsWithPage()
            /*
            if let recentWords = RecentService().listLast(){
                favoriteWords = recentWords
            }*/
        }else{
            if isHskList{
                loadHSKListWithPage(false)
            }else{
                if isPiniyns{
                    if let pinWords = PinyinService().getTexts() {
                        piniynWord = pinWords
                    }
                }else{
                    if let favWords = FavoriteService().getWordsInList(idList: favorites.id){
                        favoriteWords = favWords
                    }
                }
            }
        }
        tableView.reloadData()
    }
    
    private func loadRecentsWithPage(){
        if let recentWords = RecentService().listLast(currentPage){
            favoriteWords += recentWords
        }
        self.perform(#selector(loadTable),with: nil, afterDelay: 1.0)
        isLoaded = false
    }
    
    private func loadHSKListWithPage(_ isll: Bool){
        
        
        if let words = HSKService().getWordsFromHSKListByID(id: hskListId, page: currentPage){
            if words.isEmpty{
                return
            }
            favoriteWords += words
        }
        if isll{
            self.perform(#selector(loadTable),with: nil, afterDelay: 1.0)
            
            //tableView.beginUpdates()
            //tableView.insertSections(IndexSet(integer: currentPage), with: .automatic)
            //tableView.endUpdates()
            //tableView.reloadSections(IndexSet(integer: currentPage), with: .automatic)
        }else{
            tableView.reloadData()
        }
        
        isLoaded = false
    }
    
    @objc
    func loadTable(){
        tableView.reloadData()
    }
    
    private func removeWordFromList(selectedId: Int){
        if isRecents{
            let idWord = favoriteWords[selectedId].idWord
            RecentService().removeWordFromLast(wordId: idWord)
        }else{
            let idWord = favoriteWords[selectedId].idWord
            let idList = favorites.id
            FavoriteService().removeWordFromList(listId: idList, wordId: idWord)
        }
        
        favoriteWords.remove(at: selectedId)
        loadFavorite()
    }

}
extension FavoriteViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isPiniyns{
            return piniynWord.count
        }else{
            return favoriteWords.count
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if !isEditable{
             return nil
        }

        let deleteAction = UITableViewRowAction(style: .destructive, title: Titles.rowActionDelete) { (action, indexPathAction) in
            if self.isPiniyns{
                let currentText = self.piniynWord[indexPathAction.row]
                if PinyinService().deleteText(text: currentText){
                    
                }
                self.loadFavorite()
            }else{
                self.removeWordFromList(selectedId: indexPathAction.row)
            }
        }
        
        return [deleteAction]
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if isHskList{
            if indexPath.row == favoriteWords.count-1 && !isLoaded{
                isLoaded = true
                currentPage += 1
                print(currentPage, "PAGE")
                print(favoriteWords.count, "COUNT")
                loadHSKListWithPage(true)
            }
        }
        if isRecents{
            if indexPath.row == favoriteWords.count-1 && !isLoaded && favoriteWords.count%20==0{
                isLoaded = true
                currentPage += 1
                print(currentPage, "PAGE")
                print(favoriteWords.count, "COUNT")
                loadRecentsWithPage()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isPiniyns{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath)
            let currentPinyin = piniynWord[indexPath.row]
            cell.textLabel?.text = currentPinyin
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WordTableViewCell
            let currentWord = favoriteWords[indexPath.row]
            
            cell.initCell(word: currentWord)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isPiniyns{
            let currentPiniyn = piniynWord[indexPath.row]
            self.openPinyin(text: currentPiniyn)
        }else{
            let currentWord = favoriteWords[indexPath.row]
            self.openWord(word: currentWord)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
}
