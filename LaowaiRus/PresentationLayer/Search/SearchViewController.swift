//
//  SearchViewController.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 21.01.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
  
    
    private var searchBar = UISearchBar()
    
    private let disposeBag = DisposeBag()
    private var filteredWords = [Word]()
    private var words = [WordSearch]()
    private var isFiltered = false
    
    public var initedQuery: String = ""
    private var isEmpty = false
    
    private var emptyStrings = [
        "Извините, ничего не найдено",
        "Поиск в Baidu",
        "Поиск в 大БКРС "
    ]
    
    private var padding: CGFloat = 20
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavigationBar()
        initSearchRX()
        
        loadRecentWords()
        
        self.navigationItem.title = "Поиск"
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "WordTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.register(UINib(nibName: "EmptyTableViewCell", bundle: nil), forCellReuseIdentifier: "emptyCell")
        tableView.keyboardDismissMode = .onDrag
        
        checkStartQuery()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if isFiltered{
            // Показывает результат поиска
        }else{
            // Показывает историю поиска
            loadRecentWords()
        }
    }
    
    func checkStartQuery(){
        if !initedQuery.isEmpty{
            if #available(iOS 13.0, *) {
                searchBar.searchTextField.text = initedQuery
            } else {
                // Fallback on earlier versions
            }
            print(initedQuery,"me inited")
            loadData(query: initedQuery)
        }
        else{
            searchBar.setImage(UIImage(named: "camera"), for: .bookmark, state: .normal)
            searchBar.showsBookmarkButton = true
            
        }
    }
    
    func initNavigationBar(){
        if !initedQuery.isEmpty{
            padding = 60
        }
        let width = self.navigationController!.view.frame.width
        let height: CGFloat = 32
        let searchBarWidth = width - padding
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        
        searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: searchBarWidth, height: height))
       searchBar.placeholder = "你, hao, друг"
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.backgroundColor = .searchBarBackgroundColor
        } else {
            searchBar.textField?.backgroundColor = .searchBarBackgroundColor
        }
        searchBar.barTintColor = .searchBarTintColor
        searchBar.delegate = self
        titleView.addSubview(searchBar)
        
        self.navigationItem.titleView = titleView
    }
    
    
    func cameraTap(){
        let vc = UIStoryboard.init(name: "OCR", bundle: Bundle.main).instantiateViewController(withIdentifier: "OCRViewController") as? OCRViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func initSearchRX(){
        searchBar.rx.text
            .orEmpty
            .debounce (.milliseconds(350), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .filter { return !$0.isEmpty }
            .subscribe(onNext: { [unowned self] query in
                    self.tableView.setContentOffset(.zero, animated: true)
                self.loadData(query: query.lowercased())
            })
            .disposed(by: disposeBag)
    }
    
    func loadRecentWords(_ page: Int = 0){
        isEmpty = false
        if let lastWord = RecentService().listLast(){
            let retSearchWord = RecentService().prepareForRecentHeaders(lastWord: lastWord)
            words = retSearchWord
            isFiltered = false
            tableView.reloadData()
        }
    }
        
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        cameraTap()
    }
    
}

//MARK: - SearchQueryParser
extension SearchViewController{
    
    func loadData(query: String){
        isFiltered = true
        isEmpty = false
        let language = query.detectedLangauge()
        if language == "zh-Hans" || language == "zh-Hant" || language == "ja" { //FIND CH
            if let wordsFind = SearchService().findWordsInDiffLanguage(query: query){
                words = wordsFind
            }
        }else if language == "ru" || language == "bg" || language == "uk"{ //FIND RU
            if let wordsFind = SearchService().findWordInRu(query: query){
                words = wordsFind
                if words[0].words.isEmpty{
                    isEmpty = true
                }
            }
        }else{ //FIND EN
            if let wordsFind = SearchService().findWordByTranscription(query: query){
                words = wordsFind
                if words[0].words.isEmpty{
                    // try alternative transcription search
                    if let wordsFind = SearchService().findWordByTranscriptionAlt(query: query){
                        words = wordsFind
                         if words[0].words.isEmpty{
                            // try search by chinese characters because some characters have language property of "nul"
                            if let wordsFind = SearchService().findWordsInDiffLanguage(query: query){
                                words = wordsFind
                               if !wordsFind.isEmpty{
                                    if words[0].words.isEmpty{
                                        isEmpty = true
                                    }
                                }else{
                                    isEmpty = true
                                }
                            }
                        }
                    }
                }
            }
        }
        self.tableView.reloadData()
    }
    
}

//MARK: - UITableView Delegate
extension SearchViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return words.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return words[section].keyWord
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isEmpty{
            return emptyStrings.count
        }else{
            return words[section].words.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isEmpty {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell", for: indexPath) as! EmptyTableViewCell
            let currentTitle = emptyStrings[indexPath.row]
            cell.initCell(name: currentTitle, image: UIImage.searchButton!)
            if indexPath.row == 0{
                cell.helperImageView.image = .fuckup
            }else{
                cell.helperImageView.isHidden = false
            }
            
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WordTableViewCell
            let currentWord = words[indexPath.section].words[indexPath.row]
            
            cell.initCell(word: currentWord)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentWord = searchBar.text!
        if isEmpty{
            var url = ""
            if indexPath.row == 0{
                url = "https://www.youtube.com/watch?v=l_Sl2p9iBJE"
            }
            if indexPath.row == 1{
                url = "https://hanyu.baidu.com/s?wd=\(currentWord)"
            }
            if indexPath.row == 2{
                url = "https://bkrs.info/slovo.php?ch=\(currentWord)"
            }
            self.openUrlInSafari(urlStr: url)
        }else{
            let selectedWord = words[indexPath.section].words[indexPath.row]
            self.openWord(word: selectedWord)
        }
    }
    
}

//MARK: - SearchBar Delegate
extension SearchViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            print("me eptry")
            loadRecentWords()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                //searchBar.resignFirstResponder()
            }
        }
    }

}
