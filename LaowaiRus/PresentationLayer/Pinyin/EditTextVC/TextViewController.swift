//
//  TextViewController.swift
//  ChineseTextFlowLayout
//
//  Created by huangkun on 2019/12/16.
//  Copyright © 2019 huangkun. All rights reserved.
//

import UIKit

protocol TextViewControllerDelegate {
    func textViewControllerChanged(content: String)
}

class TextViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var delegate: TextViewControllerDelegate?
    
    var content: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        constraintInit()
        textView.font = .pinyinEditorTextViewFont
        textView.textColor = .pinyinEditorTextViewTextColor
        textView.text = content
        textView.becomeFirstResponder()
        textView.keyboardDismissMode = .onDrag
        
        let saveButton = UIBarButtonItem(title: "Готово", style: .done, target: self, action: #selector(closeAndSave))
        self.navigationItem.rightBarButtonItem = saveButton
        self.navigationItem.setHidesBackButton(true, animated:true)
    }
    
    func constraintInit(){
        NotificationCenter.default.addObserver(
                   self,
                   selector: #selector(keyboardWillShow),
                   name: UIResponder.keyboardWillShowNotification,
                   object: nil
               )
               NotificationCenter.default.addObserver(
                          self,
                          selector: #selector(keyboardWillHide),
                          name: UIResponder.keyboardWillHideNotification,
                          object: nil
                      )
    }
    
    @objc
    func closeAndSave(_ sender: UIBarButtonItem){
        content = textView.text
        delegate?.textViewControllerChanged(content: content)
        self.navigationController?.popViewController(animated: false)
    }
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            bottomConstraint.constant = keyboardHeight
        }
    }
    @objc func keyboardWillHide(_ notification: Notification) {
            bottomConstraint.constant = 0
        }
}

