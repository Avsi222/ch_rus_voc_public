//
//  ViewController.swift
//  ChineseTextFlowLayout
//
//  Created by huangkun on 2019/12/13.
//  Copyright © 2019 huangkun. All rights reserved.
//
import AVFoundation
import UIKit
import K3Base
import K3Pinyin
import FGReverser

class PinyinViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var characters = [String]()
    
    var speechSynthesizer = AVSpeechSynthesizer()
    
    var textLayout: ChineseTextFlowLayout!

    let space: CGFloat = 0
    
    var text = """
        老外欢迎您！
        吃饱了吗？
        """
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGesture()
        configureCollection()
        loadText()
    }
    
    func addGesture(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openEditView))
        self.collectionView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func configureCollection(){
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset = UIEdgeInsets.zero
        textLayout = ChineseTextFlowLayout()
        textLayout.minimumLineSpacing = space
        textLayout.minimumInteritemSpacing = space
        let estimatedSize = UICollectionViewFlowLayout.automaticSize
        textLayout.estimatedItemSize = estimatedSize
    }
    
    func loadText() {
        characters = text.tokenize()
        collectionView.reloadData()
        
        collectionView.collectionViewLayout = textLayout
    }
      
}

//MARK: - UIButton actions
extension PinyinViewController{
       
    @objc
    func openEditView(_ sender: UITapGestureRecognizer){
        if let vc = UIStoryboard.init(name: "EditorTextVC", bundle: Bundle.main).instantiateViewController(withIdentifier: "TextViewController") as? TextViewController{
            vc.content = text
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    @IBAction func searchButton(_ sender: UIBarButtonItem){
        self.openSearch(text: text)
    }
    
    
    @IBAction func speakButton(_ sender: UIBarButtonItem) {

        let textToSpeech = text
                        
        let speechUtterance = AVSpeechUtterance(string: textToSpeech)
        speechUtterance.voice = AVSpeechSynthesisVoice(language: "zh-CN")
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            } catch { // catch error
                
        }
        
        speechSynthesizer.speak(speechUtterance)
    }
    
    @IBAction func shareButton(_ sender: UIBarButtonItem) {
        
        //Rendering
        UIGraphicsBeginImageContextWithOptions(CGSize.init(width: collectionView.contentSize.width, height: collectionView.contentSize.height), false, 0)
        collectionView.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
        collectionView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let row: Double = Double(collectionView.numberOfItems(inSection: 0))
        let numberofRowthatShowinscreen = self.collectionView.contentSize.height / (self.collectionView.visibleCells.count == 1 ? 130 : 220)
        print(numberofRowthatShowinscreen)
        let scrollCount = Int(row / Double(numberofRowthatShowinscreen))

        for  i in 0..<scrollCount {
                collectionView.scrollToItem(at: IndexPath.init(row: (i+1)*Int(numberofRowthatShowinscreen), section: 0), at: .top, animated: false)
                collectionView.layer.render(in: UIGraphicsGetCurrentContext()!)
            }

        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext();
        
            //Sharing
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        let vc = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        vc.excludedActivityTypes = [.saveToCameraRoll]
        present(vc, animated: true)
        
    }
  
    @IBAction func convertButton (_ sender: UIBarButtonItem){
        text = text.fg_reversed()!
        print("convert!!!")
        loadText()
    }
    
    @IBAction func addToFavorite(_ sender: UIBarButtonItem){
        let res = PinyinService().saveText(text: text)
        if res{
            StatusBarBanner().successPinyinAddToList()
        }else{
            StatusBarBanner().alreadyInPinyinList()
        }
    }

}


extension PinyinViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return characters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PinyinCell
        let currentCharacter = characters[indexPath.row]
        
        cell.initCell(character: currentCharacter)
        
        return cell
    }
    
    
}
extension PinyinViewController: ChineseTextFlowLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout: ChineseTextFlowLayout, chineseTextForItemAt indexPath: IndexPath) -> String {
        return characters[indexPath.row]
    }
}

extension PinyinViewController: TextViewControllerDelegate{
    
    func textViewControllerChanged(content: String) {
        text = content
        loadText()
    }
}

