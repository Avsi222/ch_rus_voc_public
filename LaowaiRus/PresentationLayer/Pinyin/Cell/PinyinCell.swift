//
//  PinyinCell.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 07.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit
import K3Base
import K3Pinyin

class PinyinCell: UICollectionViewCell {
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var pinyinLabel: UILabel!
    
    func initCell(character: String){
        
        textLabel.font = .pinyinTextLabelFont
        pinyinLabel.font = .pinyinCharacterLabelFont
        
        textLabel.sizeToFit()
        textLabel.text = character
        pinyinLabel.text = character.k3.pinyin
    }
}
