//
//  TabBarController.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 02/02/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.barTintColor = .tabBarBackgroundColor
        self.tabBar.tintColor = .tabBarTintColor
        
    }

}
