//
//  StrokeViewController.swift
//  BKRS
//
//  Created by Арсений Дорогин on 10/01/2020.
//  Copyright © 2020 Арсений Дорогин. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class StrokeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    var words = [StrokeOrder]()
    var parentVC: UIViewController!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var pageController: UIPageControl!
    
    var currentpage = 0
    var findedRadical = [Word?]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        if !findedRadical.isEmpty{
            tableView.delegate = self
            tableView.dataSource = self
        }
        
        pageController.numberOfPages = words.count

        tableView.register(UINib(nibName: "WordTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeft))
        swipeLeftGesture.direction = .left
        collectionView?.addGestureRecognizer(swipeLeftGesture)
        
        
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight))
        swipeRightGesture.direction = .right
        collectionView?.addGestureRecognizer(swipeRightGesture)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("Stroke dissapear")
        //let cell = collectionView?.cellForItem(at: selectedIndexPath) as! CustomCollectionCell
        //cell.mainImageView.layer.removeAllAnimations()
    }
    
    func playButtonPress(selectedSIndexPath: IndexPath){
        let cell = collectionView?.cellForItem(at: selectedSIndexPath) as! CustomCollectionCell
              //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CustomCollectionCell

        let wordForCell = words[selectedIndexPath.row]

        cell.mainImageView.clearImage()
        cell.mainImageView.isPlay = true
        cell.mainImageView.drawWord(wordForCell)
        /*
            if cell.mainImageView.strokeIndex == wordForCell.strokes.count-1{
                
                    cell.mainImageView.isPlay = true
                    cell.mainImageView.clearImage()
                    cell.mainImageView.drawWord(wordForCell)
            }else{
                if cell.mainImageView.strokeIndex == 0{
                    
                    cell.mainImageView.isPlay = true
                    cell.mainImageView.clearImage()
                    cell.mainImageView.drawWord(wordForCell)
                }else{
                    cell.mainImageView.continueAnimation()
                }
            }*/
    }
    
    var selectedIndexPath: IndexPath = IndexPath(row: 0, section: 0)
    
    @objc
    func swipeLeft(_ sender: UISwipeGestureRecognizer){
        print("swipe left")
        if selectedIndexPath.row < words.count-1{
            tableView.reloadData()
            stopCellAnimation(indexPath: selectedIndexPath)
            selectedIndexPath.row += 1
            currentpage = selectedIndexPath.row
            pageController.currentPage = currentpage
            collectionView?.scrollToItem(at: selectedIndexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    @objc
    func swipeRight(_ sender: UISwipeGestureRecognizer){
        print("swipe right")
        if selectedIndexPath.row > 0{
            tableView.reloadData()
            stopCellAnimation(indexPath: selectedIndexPath)
            selectedIndexPath.row -= 1
            currentpage = selectedIndexPath.row
            pageController.currentPage = currentpage
            collectionView?.scrollToItem(at: selectedIndexPath, at: .centeredHorizontally, animated: true)
        }
     }
    
    func stopCellAnimation(indexPath: IndexPath){
        let cell = collectionView?.cellForItem(at: indexPath) as! CustomCollectionCell
        //cell.mainImageView.stopAnimating()
        cell.mainImageView.pauseAnimation()
    }

    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return words.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CustomCollectionCell
        
        cell.mainImageView.clearImage()
        cell.mainImageView.word = words[indexPath.row]
        //cell.mainImageView.drawWord(words[indexPath.row])
        cell.mainImageView.drawAllImage(words[indexPath.row])
    
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        playButtonPress(selectedSIndexPath: indexPath)
    }
    
}
extension StrokeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Ключ"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WordTableViewCell
            if let radical = findedRadical[currentpage]{
                tableView.isHidden = false
                cell.initCell(word: radical)
            }else{
                tableView.isHidden = true
            }
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            if let radical = findedRadical[currentpage]{
                parentVC.openWord(word: radical)
            }
    }
    }

class CustomCollectionCell: UICollectionViewCell{
    @IBOutlet weak var mainImageView: WordView!
    //@IBOutlet weak var backImageView: WordView!

}
