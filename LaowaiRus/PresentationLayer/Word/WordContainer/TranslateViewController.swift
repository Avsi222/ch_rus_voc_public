//
//  TranslateViewController.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 03.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class TranslateViewController: UIViewController {
    
    @IBOutlet weak var translateTextView: CustomTextView!

    var translateText: NSAttributedString!
    var parentVC: UIViewController!
    var isRus: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        translateTextView.delegate = self
        translateTextView.attributedText = translateText
        translateTextView.parentVC = parentVC
    }
    
}
extension TranslateViewController: UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        let newRange = NSRange(location: characterRange.location, length: characterRange.length)
        
        let word = (textView.text as NSString).substring(with: newRange)
        print(word)
        if isRus{
            if let word = WordService().findWordByWordRus(query: word){
                parentVC.openWord(word: word)
            }
        }else{
            if let word = WordService().findWordByWord(query: word){
                parentVC.openWord(word: word)
            }
        }
        return false
    }
}
