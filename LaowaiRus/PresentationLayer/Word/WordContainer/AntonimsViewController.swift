//
//  AntonimsViewController.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 03/02/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class AntonimsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var parentVC: UIViewController!
    var antonims = [Word]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "WordTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }

}
extension AntonimsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return antonims.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WordTableViewCell
        let currentAntonim = antonims[indexPath.row]
        cell.initCell(word: currentAntonim)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedAnton = antonims[indexPath.row]
        parentVC.openWord(word: selectedAnton)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
