//
//  SynonimsViewController.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 04/02/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class SynonimsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var parentVC: UIViewController!
    var synonims = [Synonims]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "WordTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }

}
extension SynonimsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return synonims.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return synonims[section].synonims.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return synonims[section].type
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WordTableViewCell
        let currentSynonim = synonims[indexPath.section].synonims[indexPath.row]
        cell.initCell(word: currentSynonim)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedSyn = synonims[indexPath.section].synonims[indexPath.row]
        parentVC.openWord(word: selectedSyn)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
