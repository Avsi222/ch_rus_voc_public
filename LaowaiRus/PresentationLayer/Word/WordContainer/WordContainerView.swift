//
//  WordContainerView.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 03.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class WordContainerView: UIView {
    
    private var currentWord: Word!
    private var parentVC: UIViewController!
    
    public func openCurrentView(selectedTitle: String, currentWord: Word,parentViewC: UIViewController){
        self.parentVC = parentViewC
        self.currentWord = currentWord
        updateView(selectedSegmentTitle: selectedTitle, currentWord: currentWord)
    }
    
    private lazy var translateViewController: TranslateViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Word", bundle: Bundle.main)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "TranslateViewController") as! TranslateViewController
        viewController.translateText = self.currentWord.combination
        viewController.parentVC = parentVC
        viewController.isRus = self.currentWord.isRus
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()
    
    
    private lazy var strokeViewController: StrokeViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Word", bundle: Bundle.main)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "StrokeViewController") as! StrokeViewController
        viewController.findedRadical = self.currentWord.radicals
        viewController.words = self.currentWord.strokes
        viewController.parentVC = parentVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()
    
    
    private lazy var antonimsViewController: AntonimsViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Word", bundle: Bundle.main)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "AntonimsViewController") as! AntonimsViewController
        viewController.antonims = self.currentWord.antonims
        viewController.parentVC = self.parentVC
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()
    
    private lazy var synonimsViewController: SynonimsViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Word", bundle: Bundle.main)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "SynonimsViewController") as! SynonimsViewController
        viewController.synonims = self.currentWord.synonims
        viewController.parentVC = self.parentVC
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()

    private lazy var exampleViewController: ExampleViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Word", bundle: Bundle.main)

        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "ExampleViewController") as! ExampleViewController
        viewController.examples = self.currentWord.examples
        viewController.searchedWord = self.currentWord.word
        viewController.parentVC = parentVC
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)

        return viewController
    }()

    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        //self.add
        //addChildViewController(viewController)

        // Add Child View as Subview
        self.addSubview(viewController.view)

        // Configure Child View
        viewController.view.frame = self.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        // Notify Child View Controller
        //viewController.didMove(toParent: self)
    }

    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)

        // Remove Child View From Superview
        viewController.view.removeFromSuperview()

        // Notify Child View Controller
        viewController.removeFromParent()
    }

    private func updateView(selectedSegmentTitle: String, currentWord: Word) {

        remove(asChildViewController: exampleViewController)
        remove(asChildViewController: translateViewController)
        remove(asChildViewController: antonimsViewController)
        remove(asChildViewController: synonimsViewController)
        remove(asChildViewController: strokeViewController)
        
        switch selectedSegmentTitle {
        case Titles.segmentTranslateTitle:
            add(asChildViewController: translateViewController)
        case Titles.segmentExamplesTitle:
            add(asChildViewController: exampleViewController)
        case Titles.segmentAntonymsTitle:
            add(asChildViewController: antonimsViewController)
        case Titles.segmentSynonymsTitle:
            add(asChildViewController: synonimsViewController)
        case Titles.segmentStrokesTitle:
            add(asChildViewController: strokeViewController)
        default:
            print("ERROROROROORO")
        }
         
    }
}
