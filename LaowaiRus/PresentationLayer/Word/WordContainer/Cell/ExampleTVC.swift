//
//  ExampleTVC.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 03.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class ExampleTVC: UITableViewCell {
    
    @IBOutlet weak var exampleLabel: CustomTextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    public func initCell(example: Example, searchedWord: String){
        let defaultText = NSMutableAttributedString(string: example.example)
        
        if let index = example.example.indexOf(target: searchedWord) {
            let range = NSRange(location: 0, length: example.example.count)
            defaultText.addAttribute(.font, value: UIFont.exampleFont, range: range)
            defaultText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.defaultTranslateWordColor, range: range)
            let highlightedRange = NSRange(location: index, length: searchedWord.count)
            defaultText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: highlightedRange)
        }
        exampleLabel.attributedText = defaultText
    }

}
