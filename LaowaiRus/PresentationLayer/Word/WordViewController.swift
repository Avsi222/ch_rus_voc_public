//
//  WordViewController.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 02/02/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit
import AVFoundation

class WordViewController: UIViewController {
    
    @IBOutlet weak var wordLabel: UILabel!
    @IBOutlet weak var transcriptionLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var mainWordView: UIView!
    @IBOutlet weak var containerView: WordContainerView!

    var currentWord: Word!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        StoreReviewHelper.incrementAppOpenedCount()
        
        initViewForWord()
        initSegmentControl()
        initBarButtons()
        initLabels()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        print("STOP ALL ACTIONS")
    }
}
//MARK: - Inits
extension WordViewController{
    
    func initViewForWord(){
        mainWordView.backgroundColor = .mainWordBGColor
        wordLabel.font = UIFont.wordLabelFont
        wordLabel.textColor = .wordWordColor
        transcriptionLabel.textColor = .transcriptWordColor
        transcriptionLabel.font = UIFont.transcriptionLabelFont
        self.navigationItem.title = currentWord.word
        self.navigationItem.titleView = UIView()
    }
    
    func initSegmentControl(){
        
    segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor :  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)], for: .selected)
            
        //segmentedControl.backgroundColor =
        segmentedControl.tintColor = UIColor.systemYellowTintColor
    segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor :  UIColor.white, NSAttributedString.Key.font : UIFont.segmentTitle], for: .normal)
        
        if #available(iOS 13.0, *) {
            segmentedControl.selectedSegmentTintColor = UIColor.systemYellowTintColor
        }
        segmentedControl.removeAllSegments()
        var currentIndex = 0
        if currentWord.isRus{
            segmentedControl.insertSegment(withTitle: Titles.segmentTranslateTitle, at: currentIndex, animated: false)
            currentIndex+=1
            if !currentWord.examples.isEmpty{
                segmentedControl.insertSegment(withTitle: Titles.segmentExamplesTitle, at: currentIndex, animated: false)
            }
        }else{
            segmentedControl.insertSegment(withTitle: Titles.segmentTranslateTitle, at: currentIndex, animated: false)
                currentIndex += 1
            if !currentWord.examples.isEmpty{
                segmentedControl.insertSegment(withTitle: Titles.segmentExamplesTitle, at: currentIndex, animated: false)
                currentIndex += 1
            }
            if !currentWord.antonims.isEmpty{
                segmentedControl.insertSegment(withTitle: Titles.segmentAntonymsTitle, at: currentIndex, animated: false)
                currentIndex += 1
            }
            if !currentWord.synonims.isEmpty{
                segmentedControl.insertSegment(withTitle: Titles.segmentSynonymsTitle, at: currentIndex, animated: false)
                currentIndex += 1
            }
            if !currentWord.strokes.isEmpty{
                segmentedControl.insertSegment(withTitle: Titles.segmentStrokesTitle, at: currentIndex, animated: false)
                currentIndex += 1
            }
            
            //segmentedControl.insertSegment(withTitle: "Черты", at: currentIndex, animated: false)
        }
        
        containerView.openCurrentView(selectedTitle: Titles.segmentTranslateTitle, currentWord: currentWord, parentViewC: self)
        segmentedControl.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
        segmentedControl.selectedSegmentIndex = 0
    }
    
    func initBarButtons(){
        
        let countingButton = UIBarButtonItem(title: currentWord.countings?.word, style: .done, target: self, action: #selector(countingButtonTap))
       countingButton.tintColor = .buttonsTintColor
           let attributes = [NSAttributedString.Key.baselineOffset: NSNumber(value: -1 ), NSAttributedString.Key.font: UIFont.countingButtonFont]
       countingButton.setTitleTextAttributes(attributes, for: .normal)
        
        let speakButton = UIBarButtonItem(image: UIImage.speakButton, style: .done, target: self, action: #selector(speakButtonTap))
        
        if currentWord.isRus == true {
            speakButton.isEnabled = false
            speakButton.tintColor = UIColor.clear
        }
        let addToFavButton = UIBarButtonItem(image: UIImage.addToFavButton, style: .done, target: self, action: #selector(addToFavTap))
        addToFavButton.tintColor = .barButtonsTintColor

        self.navigationItem.rightBarButtonItems = [addToFavButton, speakButton, countingButton]
    }
    
    func initLabels(){
        wordLabel.text = currentWord.word
        wordLabel.adjustsFontSizeToFitWidth = true
        transcriptionLabel.text = currentWord.transcription
        transcriptionLabel.adjustsFontSizeToFitWidth = true
    }
}

//MARK: - Actions
extension WordViewController{
    
    @objc
    func speakButtonTap(_ sender: UIBarButtonItem) {
        
        let speechSynthesizer = AVSpeechSynthesizer()
        let textToSpeech = wordLabel.text
        let speechUtterance = AVSpeechUtterance(string: textToSpeech!)
        speechUtterance.voice = AVSpeechSynthesisVoice(language: "zh-CN")
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
        }
        catch {
        // catch error
        }
        speechSynthesizer.speak(speechUtterance)
    }
    
    @objc
    func countingButtonTap(_ sender: UIButton){
        if let countingWord = currentWord.countings {
            openWord(word: countingWord)
        }
    }
    
    @objc
    func addToFavTap(_ sender: UIBarButtonItem){
        showAlertAddToFav()
    }
    
    func showAlertAddToFav(){
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            let alert = CustomBottomSheet(style: .alert)
            alert.initController(vc: self, word: currentWord)
            self.present(alert,animated: true)
        }else{
            let alert = CustomBottomSheet()
            alert.initController(vc: self, word: currentWord)
            self.present(alert,animated: true)
        }
    }
}

//MARK: - SegmentChange
extension WordViewController{
    
    @objc
    func selectionDidChange(_ sender: UISegmentedControl){
        if let titleForSegment = sender.titleForSegment(at: sender.selectedSegmentIndex){
            containerView.openCurrentView(selectedTitle: titleForSegment, currentWord: currentWord, parentViewC: self)
        }
    }
}
