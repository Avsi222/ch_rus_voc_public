//
//  CustomNavigationBar.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 21/01/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UINavigationBar.appearance().barTintColor = .navBarBackgroundColor
        UINavigationBar.appearance().tintColor = .navBarTintColor
    }

}
