//
//  OCRViewController.swift
//
//  Created by Митя Полянский + Арсений Дорогин
//

import UIKit
import AVFoundation
import AudioToolbox
import SwiftyTesseract
import SwiftyTesseractRTE
import CropViewController
import RLBAlertsPickers

protocol OCRSearchProtocol {
    func openFindedQuery(query: String)
}

class OCRViewController: UIViewController, CropViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var regionOfInterest: UIView!
    @IBOutlet weak var regionOfInterestWidth: NSLayoutConstraint!
    @IBOutlet weak var regionOfInterestHeight: NSLayoutConstraint!
    
    @IBOutlet weak var pinyinOutlet: UIBarButtonItem!
    @IBOutlet weak var searchOutlet: UIBarButtonItem!
    @IBOutlet weak var copyOutlet: UIBarButtonItem!
    @IBOutlet weak var choosePhotoOutlet: UIBarButtonItem!
    
    let swiftyTesseract = SwiftyTesseract(language: .chineseSimplified)
    let child = SpinnerViewController()
    var delegate: OCRSearchProtocol?
    
    private var image: UIImage?
    private var croppingStyle = CropViewCroppingStyle.default
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
    
    var engine: RealTimeEngine!
    var excludeLayer: CAShapeLayer!
    var recognitionButton: UIButton!
    var recognitionTitleLabel: UILabel!
    var recognitionLabel: UITextView!
    var searchButton: UIButton!
    var data: String!
    var recognitionIsRunning = false {
        didSet {
            let recognitionButtonText = recognitionIsRunning ?  "Остановить" : "Начать распознавание"
            let recognitionButtonColor = recognitionIsRunning ?  UIColor.red : .green
            DispatchQueue.main.async { [weak self] in
                self?.recognitionButton.setTitle(recognitionButtonText, for: .normal)
                self?.recognitionButton.setTitleColor(recognitionButtonColor, for: .normal)
            }
            engine.recognitionIsActive = recognitionIsRunning
        }
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        copyOutlet.tintColor = .systemYellowTintColor
        pinyinOutlet.tintColor = .systemYellowTintColor
        searchOutlet.tintColor = .systemYellowTintColor
        choosePhotoOutlet.tintColor = .systemYellowTintColor
        self.tabBarController?.tabBar.isHidden = true
        initRecognationController()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "flashlight"), style: .done, target: self, action: #selector(flashlight(sender:)))
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        
        regionOfInterest.addGestureRecognizer(panGesture)
        regionOfInterest.layer.borderWidth = 1.0
        regionOfInterest.layer.borderColor = UIColor(named: "buttonColor")?.cgColor
        regionOfInterest.backgroundColor = .clear
        
        excludeLayer = CAShapeLayer()
        excludeLayer.fillRule = .evenOdd
        excludeLayer.fillColor = UIColor.black.cgColor
        excludeLayer.opacity = 0.7
    
        initEngineController()
    }

    override func viewDidLayoutSubviews() {
        engine.bindPreviewLayer(to: previewView)
        engine.regionOfInterest = regionOfInterest.frame
        previewView.layer.addSublayer(regionOfInterest.layer)
        fillOpaqueAroundAreaOfInterest(parentView: previewView, areaOfInterest: regionOfInterest)
    }
  
    private func fillOpaqueAroundAreaOfInterest(parentView: UIView, areaOfInterest: UIView) {
            let parentViewBounds = parentView.bounds
            let areaOfInterestFrame = areaOfInterest.frame
            
            let path = UIBezierPath(rect: parentViewBounds)
            let areaOfInterestPath = UIBezierPath(rect: areaOfInterestFrame)
            path.append(areaOfInterestPath)
            path.usesEvenOddFillRule = true
            
            excludeLayer.path = path.cgPath
            parentView.layer.addSublayer(excludeLayer)
    }
        
    //MARK: Picker Image View
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           guard let image = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) else { return }
           
           let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
           cropController.delegate = self
           cropController.title = "Выделите нужный текст"
           cropController.doneButtonTitle = "Распознать"
           self.image = image
           
           if croppingStyle == .circular {
               if picker.sourceType == .camera {
                   picker.dismiss(animated: true, completion: {
                       self.present(cropController, animated: true, completion: nil)
                   })
               } else {
                   picker.pushViewController(cropController, animated: true)
               }
           }
           else { //otherwise dismiss, and then present from the main controller
               picker.dismiss(animated: true, completion: {
                   self.present(cropController, animated: true, completion: nil)
               })
           }
    }
       
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
       
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
       
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
                
         cropViewController.addChild(self.child)
                       self.child.view.frame = self.view.frame
                       cropViewController.view.addSubview(self.child.view)
                       self.child.didMove(toParent: self)
                DispatchQueue.main.async {
                   self.swiftyTesseract.performOCR(on: image) { recognizedString in
                   AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                   self.recognitionLabel.text = recognizedString
                    }
                   cropViewController.dismiss(animated: true, completion: nil)
               }
    }
    
    // MARK: Navigation
    func openGallery(){
        self.croppingStyle = .default
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func openCamera(){
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
     
    
     @objc public func flashlight(sender: AnyObject) {
       guard let device = AVCaptureDevice.default(for: .video) else { return }
       if device.hasTorch {
         do {
           try device.lockForConfiguration()
           device.torchMode = device.torchMode == .off ? .on : .off
           device.unlockForConfiguration()
           if device.torchMode == .on{ navigationItem.rightBarButtonItem!.image = UIImage(named: "flashlightOn")
           }
           if device.torchMode == .off{
               navigationItem.rightBarButtonItem!.image = UIImage(named: "flashlight")
           }
         } catch let e {
           print("Error: \(e.localizedDescription)")
         }
       }
       }
    
  
    @objc func handlePan(_ sender: UIPanGestureRecognizer) {
        let translate = sender.translation(in: regionOfInterest)
        
        UIView.animate(withDuration: 0) {
          self.regionOfInterestWidth.constant += translate.x
          self.regionOfInterestHeight.constant += translate.y
        }
        
        sender.setTranslation(.zero, in: regionOfInterest)
        viewDidLayoutSubviews()
       
    }
  
    @objc func recognitionButtonTapped(_ sender: Any) {
        recognitionIsRunning.toggle()
    }
  
    func textFieldShouldReturn(_ recognitionLabel: UITextView) -> Bool {
        recognitionLabel.resignFirstResponder()
        return true
    }
    
    
}

//MARK: - Buttons Action
extension OCRViewController {

    @IBAction func searchButton(_ sender: UIBarButtonItem) {
        guard let text = recognitionLabel.text else { return }
        self.openSearch(text: text)
    }
    
    @IBAction func photoOpen(_ sender: UIBarButtonItem) {

        var alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        } else{
            alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        }
            
        alert.addAction(image: UIImage(named: "camera"), title: "Сделать фото", color: UIColor(named: "alertTextColor"), style: .default) { action in
                    self.openCamera()
        }
        
        alert.addAction(image: UIImage(named: "photo"), title: "Выбрать фото", color: UIColor(named: "alertTextColor"), style: .default) { action in
                    self.openGallery()
        }
        alert.addAction(UIAlertAction.init(title: "Отмена", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
        
    @IBAction func pinyinOpen(_ sender: UIBarButtonItem) {
        openPinyin(text: recognitionLabel.text)
    }
        
    
    @IBAction func copyCat(_ sender: UIBarButtonItem) {
        UIPasteboard.general.string = recognitionLabel.text
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        StatusBarBanner().successCopiedInBuffer()
    }
}

//MARK: - init's functions
extension OCRViewController{
    
    //MARK: RealTimeEngine Setup
    func initEngineController(){
        
        engine = RealTimeEngine(swiftyTesseract: swiftyTesseract, desiredReliability: .verifiable) { [weak self] recognizedString in
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
               
            DispatchQueue.main.async {
                self?.recognitionLabel.text = recognizedString
            }
            self?.recognitionIsRunning = false
        }
        
        engine.recognitionIsActive = false
        engine.cameraQuality = .high
        engine.startPreview()
    }
    
    func initRecognationController(){
        
        recognitionButton = UIButton()
        recognitionButton.setTitleColor(.green, for: .normal)
        recognitionButton.setTitle("Начать распознавание", for: .normal)
          
        recognitionButton.addTarget(self, action: #selector(recognitionButtonTapped(_:)), for: .touchUpInside)
      
        recognitionTitleLabel = UILabel()
        recognitionTitleLabel.text = "Результат:"
        recognitionTitleLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        recognitionTitleLabel.textColor = .white
      
        let stackView = UIStackView(arrangedSubviews: [recognitionButton, recognitionTitleLabel])
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 8.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
      
        view.addSubview(stackView)
        stackView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        stackView.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor).isActive = true
      
        let recognitionWidth = view.frame.width
      
        recognitionLabel = UITextView(frame: CGRect(x: view.frame.width/2 -  recognitionWidth/2,y:view.frame.height/2+50, width: recognitionWidth, height: 150))
        recognitionLabel.textAlignment = .center
        recognitionLabel.textColor = .white
        _ = recognitionLabel.heightAnchor.constraint(equalToConstant: 100)
        recognitionLabel.text = "你好，朋友！"
        
        //    我爱中国
        recognitionLabel.backgroundColor = .clear
        recognitionLabel.font = .systemFont(ofSize: 24)
        recognitionLabel.isEditable = false
        view.addSubview(recognitionLabel)
    }

}
