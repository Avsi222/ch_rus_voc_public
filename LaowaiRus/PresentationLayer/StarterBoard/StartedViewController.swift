//
//  StartedViewController.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 18.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit


class StarterViewController: UIViewController {
    
    var swiftyOnboard: SwiftyOnboard!
    let colors:[UIColor] = [UIColor(red: 43/255, green: 53/255, blue: 66/255, alpha: 1.0),UIColor(red: 87/255, green: 96/255, blue: 111/255, alpha: 1.0),UIColor(red: 236/255, green: 204/255, blue: 104/255, alpha: 1.0)]
    let textColors: [UIColor] = [UIColor.white, UIColor.white, UIColor(red: 43/255, green: 53/255, blue: 66/255, alpha: 1.0)]
    var titleArray: [String] = ["Добро пожаловать в Лаовай!", "Китайский язык в кармане", "Полезные функции"]
    var subTitleArray: [String] = ["Универсальный поиск даёт возможность\n искать как по словам,\n так и по целым словосочетаниям.", "Переводы, примеры, синонимы, антонимы, счётные слова теперь не нужно искать по всему интернету.\n\n Всё это теперь всегда с Вами!", "Сканируйте текст, узнавайте пиньинь,\nслушайте произношение!\n\n Спасибо за покупку, надеемся Вам понравится:)"]
    
    var gradiant: CAGradientLayer = {
        //Gradiant for the background view
        let black = UIColor(red: 43/255, green: 53/255, blue: 66/255, alpha: 1.0).cgColor
        let yellow = UIColor(red: 236/255, green: 204/255, blue: 104/255, alpha: 1.0).cgColor
        let gradiant = CAGradientLayer()
        gradiant.colors = [black, yellow]
        gradiant.startPoint = CGPoint(x: 0.5, y: 0.18)
        return gradiant
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gradient()
        
        swiftyOnboard = SwiftyOnboard(frame: view.frame, style: .light)
        view.addSubview(swiftyOnboard)
        swiftyOnboard.dataSource = self
        swiftyOnboard.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func gradient() {
        //Add the gradiant to the view:
        self.gradiant.frame = view.bounds
        view.layer.addSublayer(gradiant)
    }
    
    @objc func handleSkip() {
        swiftyOnboard?.goToPage(index: 2, animated: true)
    }
    
    @objc func handleContinue(sender: UIButton) {
        let index = sender.tag
        if index == 2{
            goToVocalbury()
        }else{
            swiftyOnboard?.goToPage(index: index + 1, animated: true)
        }
    }
    
    func goToVocalbury(){
        let tabbarVC = UIStoryboard.init(name: "TabBar", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBarController")
            as? TabBarController
        tabbarVC!.modalPresentationStyle = .fullScreen
        self.present(tabbarVC!,animated: true)
    }
}

extension StarterViewController: SwiftyOnboardDelegate, SwiftyOnboardDataSource {
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
        //Number of pages in the onboarding:
        return 3
    }
    
    func swiftyOnboardBackgroundColorFor(_ swiftyOnboard: SwiftyOnboard, atIndex index: Int) -> UIColor? {
        //Return the background color for the page at index:
        return colors[index]
    }
    
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        let view = SwiftyOnboardPage()
        
        //Set the image on the page:
        view.imageView.image = UIImage(named: "onboard\(index)")
        view.title.textColor! = textColors[index]
        view.subTitle.textColor! = textColors[index]
        //Set the font and color for the labels:
        view.title.font = UIFont.systemFont(ofSize: 22)
        view.subTitle.font = UIFont.systemFont(ofSize: 16)
        
        //Set the text in the page:
        view.title.text = titleArray[index]
        view.subTitle.text = subTitleArray[index]
        
        //Return the page for the given index:
        return view
    }
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
        let overlay = SwiftyOnboardOverlay()
        
        //Setup targets for the buttons on the overlay view:
        overlay.skipButton.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
        overlay.continueButton.addTarget(self, action: #selector(handleContinue), for: .touchUpInside)
        
        //Setup for the overlay buttons:
        overlay.continueButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        overlay.continueButton.setTitleColor(UIColor.white, for: .normal)
        overlay.skipButton.setTitleColor(UIColor.white, for: .normal)
        overlay.skipButton.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        
        //Return the overlay view:
        return overlay
    }
    
    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: SwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
        let currentPage = round(position)
        overlay.pageControl.currentPage = Int(currentPage)
        print(Int(currentPage))
        overlay.continueButton.tag = Int(position)
        
        if currentPage == 0.0 || currentPage == 1.0 {
            overlay.continueButton.setTitle("Далее", for: .normal)
            overlay.skipButton.setTitle("Пропустить", for: .normal)
            overlay.skipButton.isHidden = false
            overlay.continueButton.setTitleColor(UIColor.white, for: .normal)
                   overlay.skipButton.setTitleColor(UIColor.white, for: .normal)
        } else {
            overlay.continueButton.setTitle("Поехали!", for: .normal)
            overlay.skipButton.isHidden = true
            overlay.continueButton.setTitleColor(UIColor.black, for: .normal)
            overlay.skipButton.setTitleColor(UIColor.black, for: .normal)
        }
    }
}



