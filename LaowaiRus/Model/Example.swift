//
//  Example.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 03.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit


class Example{
    var id: Int = 0
    var example: String = ""
    
    init(id: Int, example: String) {
        self.id = id
        var endExample = example
        endExample.removeLast() //удаляем ентр в конце примеров
        self.example = endExample
    }
}
