//
//  Antonims.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 03/02/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class Antonims {
    var id: Int = 0
    var word: String = ""
    var antonims = [Word]()
    
    init(id: Int, word: String, antonims: String) {
        self.id = id
        self.word = word
        let antonimsArray = antonims.components(separatedBy: ";")
        for word in antonimsArray{
            if let selectedWord = WordService().findWordByID(wordID: word){
                self.antonims.append(selectedWord)
            }
        }
    }
}
