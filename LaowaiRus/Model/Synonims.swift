//
//  Synonims.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 04/02/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class Synonims {
    var type: String = ""
    var synonims = [Word]()
    var synonimsString = [String]()
    
    init(type: String, synonims: String, wordQuery: String) {
        self.type = type
        var synRemoving = synonims
        synRemoving.removeFirst()
        synRemoving.removeLast()
        var synsArray = synRemoving.components(separatedBy: ";")
        if let index = synsArray.firstIndex(of: wordQuery) {
            synsArray.remove(at: index)
        }
        self.synonimsString = synsArray
    }
    
    init(type: String, synonims: [String]) {
        self.type = type
        
        for oneWord in synonims{
            if let selectedWord = WordService().findWordByWord(query: oneWord){
                self.synonims.append(selectedWord)
            }
        }
        //self.synonims = synonims
    }
}
