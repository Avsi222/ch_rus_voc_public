//
//  Word.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 21.01.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class Word {
    
    var idWord: Int = -1
    var word: String = ""
    var translate: String = ""
    var transcription: String = ""
    var examples = [Example]()
    var antonims = [Word]()
    var synonims = [Synonims]()
    var countings: Word?
    
    var radicals = [Word?]()
    var strokes = [StrokeOrder]()
    
    var timeAppend: Double = 0
    
    var isRus: Bool = false
    
    var combination = NSMutableAttributedString()
    
    init(idWord: Int, word: String, translate: String, transcription: String){
        self.idWord = idWord
        self.word = word
        self.translate = translate
        self.transcription = transcription
        if transcription.isEmpty{
            isRus = true
        }else{
            isRus = false
        }
        convertToJson()
    }
    
    public func findCountings(){
        if let countings = CountingService().loadWords(queryWord: word){
            self.countings = countings
        }
    }
    
    public func findRadicals(){
        for item in self.word{
            if let radicalId = SearchService().findCHWordIdByWord(query: String(item)){
                if let radical = WordService().getKeyRadicalForWord(wordID: radicalId){
                    radicals.append(radical)
                }else{
                    radicals.append(nil)
                }
            }
        }
    }
    
    public func findStrokes(){
        for item in self.word{
            if let result = WordService().findAndDraw(charForFind: String(item)){
                strokes.append(result)
            }
        }
    }
    
    public func findAntonims(){
        if let antonims = AntonimsService().findAntonimsForWord(queryWord: word){
            self.antonims = antonims.antonims
        }
    }
    
    public func findSynonims(){
        if let synonims = SynonimsService().findSynonimsForWord(queryWord: word){
            self.synonims = synonims
        }
    }
    
    public func findExamples(){
        
        if let examples = SearchService().findExampleForWord(queryWord: word){
            self.examples = examples
        }
    }
    
    private func convertToJson(){
        let data = translate.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
            {
                let sotrJsonArray = jsonArray.sorted { (first, second) -> Bool in
                    var fir = first.key
                    var sec = second.key
                    fir.removeLast()
                    sec.removeLast()
                    let intFir = fir
                    let intSec = sec
                    return Int(intFir)! < Int(intSec)!
                }
                for elem in sotrJsonArray{
                    let form = formatedString(key: elem.key, value: elem.value as! String)
                    if form.colorText != nil{
                        combination.append(form.colorText)
                    }
                    if form.defaultText != nil{
                        combination.append(form.defaultText)
                    }
                    if form.boldText != nil{
                        combination.append(form.boldText)
                    }
                    if form.exampleText != nil{
                        combination.append(form.exampleText)
                    }
                    if form.italicText != nil{
                        combination.append(form.italicText)
                    }
                    if form.linkText != nil{
                        combination.append(form.linkText)
                    }
                }

                /*
                if let index = combination.string.indexOf(target: word) {
                    let highlightedRange = NSRange(location: index, length: word.count)
                    combination.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue, range: highlightedRange)
                }*/  //for highlited searched word in translate
                
            } else {
                AppDelegate().showError(message: Errors.parsingError)
            }
        } catch let error as NSError {
            AppDelegate().showError(message: error.localizedDescription)
            
            if !translate.isEmpty {
                let defaultText = NSMutableAttributedString(string: translate)
                let range = NSRange(location: 0, length: translate.count)
                defaultText.addAttribute(.font, value: UIFont.defaultTranslateFont, range: range)
                combination.append(defaultText)
            }
            
        }
    }
}
