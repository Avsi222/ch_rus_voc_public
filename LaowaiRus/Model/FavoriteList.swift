//
//  FavoriteList.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 21.01.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class FavoriteList{
    var id:Int = 0
    var name:String = ""
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
}
