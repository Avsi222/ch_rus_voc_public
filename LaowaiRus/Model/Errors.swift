//
//  Errors.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 21.01.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class Errors{
    
    static let databaseError: String = "Ошибка подключения базы данных"
    
    static let favoriteAppendError: String = "Ошибка добавления в избранное"
    
    static let favoriteIsAddError: String = "Такое слово уже есть в избранном"
    
    static let queryError: String = "Ошибка запроса"
    
    static let parsingError: String = "Ошибка при парсинге"
}
