//
//  WordSearch.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 06.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import Foundation

class WordSearch {
    var keyWord = ""
    var words = [Word]()
    
    init(words: [Word]) {
        self.words = words
    }
    
    init(words: [Word],keyS: String) {
        self.words = words
        self.keyWord = keyS
    }
}
