//
//  FormatedString.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 21.01.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class formatedString{
    var colorText : NSMutableAttributedString!
    var defaultText : NSMutableAttributedString!
    var boldText : NSMutableAttributedString!
    var exampleText : NSMutableAttributedString!
    var italicText : NSMutableAttributedString!
    var linkText : NSMutableAttributedString!
    
    init(key: String, value: String) {
        if key.contains("s") && !value.contains("•"){
            
            let textWord = value
            defaultText = NSMutableAttributedString(string: textWord + " ")
            let range = NSRange(location: 0, length: textWord.count+1)
            defaultText.addAttribute(.font, value: UIFont.defaultTranslateFont,  range: range)
            defaultText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.defaultTranslateWordColor, range: range)
        }
        if key.contains("c"){
            let colorTextWord = value
            colorText = NSMutableAttributedString(string : "" + colorTextWord + " ")
            let range = NSRange(location: 0, length: colorTextWord.count+1)
            colorText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.colorTranslateWordColor , range: range)
            colorText.addAttribute(.font, value: UIFont.colorTranslateFont, range: range)
        }
        if key.contains("i"){
            let textWord = value
            italicText = NSMutableAttributedString(string: textWord + " ")
            let range = NSRange(location: 0, length: textWord.count+1)
            italicText.addAttribute(.font, value: UIFont.italicTranslateFont, range: range)
            italicText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.italicTranslateWordColor, range: range)
        }
        if key.contains("b"){
            let colorTextWord = value
            boldText = NSMutableAttributedString(string : "" + colorTextWord + " ")
            let range = NSRange(location: 0, length: colorTextWord.count+1)
            boldText.addAttribute(NSAttributedString.Key.font, value: UIFont.boldTranslateFont, range: range)
            boldText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.boldTranslateWordColor, range: range)
        }
        if key.contains("x") || value.contains("•"){
            let colorTextWord = value
            exampleText = NSMutableAttributedString(string : colorTextWord + " ")
            let range = NSRange(location: 0, length: colorTextWord.count+1)
            exampleText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.exampleTranslateWordColor , range: range)
            exampleText.addAttribute(.font, value: UIFont.exampleTranslateFont, range: range)
        }
        
        
        if key.contains("r"){
            let colorTextWord = value
            linkText = NSMutableAttributedString(string : "" + colorTextWord + " ")
            var dopLen = 1
            if colorTextWord.contains(")"){
                dopLen+=2
            }
            let range = NSRange(location: 0, length: colorTextWord.count)
            linkText.addAttribute(NSAttributedString.Key.link, value: "ref", range: range)
            
            let range2 = NSRange(location: 0, length: colorTextWord.count)
            linkText.addAttribute(.font, value: UIFont.linkTranslateFont,  range: range2)
            linkText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.linkTranslateWordColor, range: range2)
        }
    }
    
    
}
