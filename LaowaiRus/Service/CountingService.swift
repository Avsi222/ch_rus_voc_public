//
//  CountingService.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 11.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class CountingService: QueryService {
    
    func loadWords(queryWord: String) -> Word? {
        database = SQLService.sharedInstance.database
        do {
            let sqlquery = "SELECT schWord FROM SchetnieWord WHERE word = \"\(queryWord)\""
            print(sqlquery)
            let queryWords = try self.database.prepare(sqlquery)
            let wordFinded = parseRowCountings(wordsQuery: queryWords)
            return wordFinded
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
}
