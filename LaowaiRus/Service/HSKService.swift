//
//  HSKService.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 10.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import Foundation

class HSKService: QueryService {
    
    private let wordCount = 60
    
    public func getWordsFromHSKListByID(id: Int, page: Int) -> [Word]? {
        
        let sqlQuery = "SELECT * FROM HSK\(id) WHERE ROWID < \(page*wordCount) and ROWID > \((page-1)*wordCount)"
        print(sqlQuery)
        return getHSKLists(query: sqlQuery)
    }
    
    private func getHSKLists(query: String) -> [Word]? {
        
        database = SQLService.sharedInstance.database
        do {
            let queryWords = try self.database.prepare(query)
            let words = parseRowHSKLists(wordsQuery: queryWords)
            return words
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
    
}
