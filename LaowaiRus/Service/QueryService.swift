//
//  QueryService.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 21.01.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit
import SQLite

class QueryService: SQLService{
    
    private var queryReturnCount: Int = 100
    
    public func parseRowHSKLists(wordsQuery: Statement) -> [Word]?{
        var words = [Word]()
        for row in wordsQuery {
            let idWordRow2 = row[0] as! Int64
            let idWordRow = Int(idWordRow2)
            let wordRow = row[1] as! String
            let transcriptionRow = row[2] as! String
            let translateRow = row[3] as! String
            let word = Word(idWord: idWordRow, word: wordRow, translate: translateRow, transcription: transcriptionRow)
            words.append(word)
        }
        return words
    }
    
    public func parseRowDiffLang(wordsQuery: Statement) -> [Word]{
        var index = 0
        var words = [Word]()
        for row in wordsQuery {
            if index >= queryReturnCount{
                break
            }
            index += 1
            let idWordRow2 = row[0] as! Int64
            let idWordRow = Int(idWordRow2)
            
            if let (translateRow,transcriptionRow) = SearchService().findCHWordById(id: idWordRow) {
                let wordRow = row[1] as! String
                let word = Word(idWord: idWordRow, word: wordRow, translate: translateRow, transcription: transcriptionRow)
                words.append(word)
            }
        }
        return words
    }
    
    public func parseRowForIdSearch(wordsQuery: Statement) -> Word? {
        for row in wordsQuery {
            let idWordRow2 = row[0] as! Int64
            let idWordRow = Int(idWordRow2)
            let wordRow = row[1] as! String
            let transcriptionRow = row[2] as! String
            let translateRow = row[3] as! String
            let word = Word(idWord: idWordRow, word: wordRow, translate: translateRow, transcription: transcriptionRow)
            return word
        }
        return nil
    }
    
    public func TESTparseRowForIdSearch(wordsQuery: Statement) -> [Word] {
        var words = [Word]()
        for row in wordsQuery {
            let idWordRow2 = row[0] as! Int64
            let idWordRow = Int(idWordRow2)
            let wordRow = row[1] as! String
            let transcriptionRow = row[2] as! String
            let translateRow = row[3] as! String
            let word = Word(idWord: idWordRow, word: wordRow, translate: translateRow, transcription: transcriptionRow)
            words.append(word)
        }
        return words
    }
    
    public func parseRowRus(wordsQuery: Statement) -> [Word]{
        var index = 0
        var words = [Word]()
        for row in wordsQuery {
            if index >= queryReturnCount{
                break
            }
            index += 1
            
            let idWordRow2 = row[0] as! Int64
            let idWordRow = Int(idWordRow2)
            
            if let translateRow = SearchService().findRUWordById(id: idWordRow) {
                let wordRow = row[1] as! String
                let word = Word(idWord: idWordRow, word: wordRow, translate: translateRow, transcription: "")
                words.append(word)
            }
            
        }
        return words
    }
    
    public func parseRowExamples(wordsQuery: Statement) -> [Example]{
        var examples = [Example]()
        for row in wordsQuery {
            
            let idExampleRow2 = row[0] as! Int64
            let idExampleRow = Int(idExampleRow2)
            let exampleRowText = row[1] as! String
            let example = Example(id: idExampleRow, example: exampleRowText)
            examples.append(example)
            
        }
        return examples
    }
    
    public func parseRowAntonims(wordsQuery: Statement) -> Antonims?{
        var antonim: Antonims!
        for row in wordsQuery {
            let id = row[0] as! Int64
            let id2 = Int(id)
            let word = row[1] as! String
            let antonimsForWord = row[2] as! String
            antonim = Antonims(id: id2, word: word, antonims: antonimsForWord)
            break
        }
        return antonim
    }
    
    public func parseRowCountings(wordsQuery: Statement) -> Word?{
        
        for row in wordsQuery {
            let word = row[0] as! String
            if let wordFinded = SearchService().findCHWordByWord(query: word){
                return wordFinded
            }
        }
        return nil
    }
    
    public func parseRowSynonims(wordsQuery: Statement, queryWord: String) -> [Synonims]?{
        var synonims = [Synonims]()
        for row in wordsQuery {
            let type = row[1] as! String
            let synonimsForWord = row[2] as! String
            let synonim = Synonims(type: type, synonims: synonimsForWord, wordQuery: queryWord)
            synonims.append(synonim)
        }
        return synonims
    }
}
