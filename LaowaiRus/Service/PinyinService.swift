//
//  PinyinService.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 12.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import SQLite

class PinyinService: SQLService {
    
    func saveText(text: String) -> Bool {
        dbRecFav = SQLService.sharedInstance.dbRecFav
        let now = Double(Date().timeIntervalSince1970)
        do{
            let checkText = pinyinFavTable.filter(self.word == text)
            let count = try dbRecFav.scalar(checkText.count)
            if count == 0{
                let insert = pinyinFavTable.insert(self.word <- text, self.timeInsert <- now)
                try dbRecFav.run(insert)
                return true
            }else{
                return false
            }
        } catch {
            AppDelegate().showError(message: error.localizedDescription)
        }
           
        
        return false
    }
    
    func deleteText(text: String) -> Bool {
        dbRecFav = SQLService.sharedInstance.dbRecFav
        do{
            let remove = pinyinFavTable.filter(self.word == text)
            try dbRecFav.run(remove.delete())
            return true
        } catch {
            AppDelegate().showError(message: error.localizedDescription)
        }
        return false
    }
    
    func getTexts() -> [String]? {
         
        dbRecFav = SQLService.sharedInstance.dbRecFav
        do {
            let orderTable = pinyinFavTable.order(timeInsert.asc)
            let queryWords = try dbRecFav.prepare(orderTable)
            var words = [String]()
            for row in queryWords {
                words.append(row[word])
            }
            return words.reversed()
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
}
