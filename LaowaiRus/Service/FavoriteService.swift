//
//  FavoriteService.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 21.01.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit
import SQLite

class FavoriteService: SQLService{
    
    public func getListFav() -> [FavoriteList]? {
        dbRecFav = SQLService.sharedInstance.dbRecFav
        do {
            let queryResult = try self.dbRecFav.prepare(favoriteListTable)
            var lists = [FavoriteList]()
            for row in queryResult {
                let listId = row[self.listId]
                let listName = row[self.listName]
                let list = FavoriteList(id: listId, name: listName)
                lists.append(list)
            }
            return lists.reversed()
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.databaseError)
            return nil
        }
    }
    
    func addNewFavoriteList(name: String, id: Int){
        dbRecFav = SQLService.sharedInstance.dbRecFav
        do{
            let insert = favoriteListTable.insert(self.listId <- id, self.listName <- name)
            try dbRecFav.run(insert)
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
        }
    }
    
    func addWordToFavoriteList(word: Word,list: Int) -> Bool{
        dbRecFav = SQLService.sharedInstance.dbRecFav
        if checkWordInList(word: word, list: list){
            do{
                let insert = favoriteTable.insert(self.listId <- list, self.word <- word.word, self.translate <- word.translate, self.transcription <- word.transcription, self.wordId <- word.idWord)
                try dbRecFav.run(insert)
                return true
            } catch {
                print(error)
                AppDelegate().showError(message: Errors.queryError)
                return false
            }
        }else{
            return false
        }
    }
    
    func checkWordInList(word: Word,list: Int) -> Bool{
        dbRecFav = SQLService.sharedInstance.dbRecFav
        do{
            let query = favoriteTable.where(self.listId == list && self.wordId == word.idWord)
            let queryResult = try self.dbRecFav.prepare(query)
            if queryResult.shuffled().count == 0{
                return true
            }else{
                return false
            }
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return false
        }
        
        
    }
    
    func removeList(listId: Int){
        dbRecFav = SQLService.sharedInstance.dbRecFav
         do{
            let remove = favoriteListTable.filter(self.listId == listId)
            try dbRecFav.run(remove.delete())
            
            let remove2 = favoriteTable.filter(self.listId == listId)
            try dbRecFav.run(remove2.delete())
        } catch {
            print(error)
            
        }
    }
    
    func editList(newName: String, listId: Int){
        dbRecFav = SQLService.sharedInstance.dbRecFav
         do{
            let updateTable = favoriteListTable.filter(self.listId == listId)
            try dbRecFav.run(updateTable.update(self.listName <- newName))
        } catch {
            print(error)
            
        }
    }
    
    func removeWordFromList(listId: Int,wordId: Int){
        dbRecFav = SQLService.sharedInstance.dbRecFav
         do{
            let remove = favoriteTable.filter(self.listId == listId && self.wordId == wordId)
            try dbRecFav.run(remove.delete())
        } catch {
            print(error)
            
        }
    }
    
    func getWordsInList(idList: Int) -> [Word]?{
        dbRecFav = SQLService.sharedInstance.dbRecFav
        do {
            let query = favoriteTable.where(self.listId == idList)
            let users = try self.dbRecFav.prepare(query)
            var words = [Word]()
            for row in users {
                let word = Word(idWord: row[self.wordId], word: row[self.word], translate: row[self.translate], transcription: row[self.transcription])
                words.append(word)
            }
            return words.reversed()
        } catch {
            print(error)
            return nil
        }
    }
}
