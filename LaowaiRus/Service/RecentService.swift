//
//  RecentService.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 03.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit
import SQLite

class RecentService: SQLService {
    
    func saveToLast(word: Word){
        dbRecFav = SQLService.sharedInstance.dbRecFav
        let now = Double(Date().timeIntervalSince1970)
        do{
        
            let insert = recentTable.insert(self.word <- word.word, translate <- word.translate, transcription <- word.transcription, wordId <- word.idWord, timeInsert <- now)
            try dbRecFav.run(insert)
        } catch {
            do{
                let filteredRecent = recentTable.filter(wordId == word.idWord)
                let count = try dbRecFav.scalar(filteredRecent.count)
                if count == 0{
                    AppDelegate().showError(message: error.localizedDescription)
                }else{
                    let update = filteredRecent.update(timeInsert <- now)
                    try dbRecFav.run(update)
                }
            } catch{
                AppDelegate().showError(message: error.localizedDescription)
            }
           
        }
           
    }
       
    func removeWordFromLast(wordId: Int){
        dbRecFav = SQLService.sharedInstance.dbRecFav
         do{
            let remove = recentTable.filter(self.wordId == wordId)
            try dbRecFav.run(remove.delete())
        } catch {
            print(error)
        }
    }
    
    var limitElementOnPage = 40 //поменяй здесь количество
    
    func listLast(_ page: Int = 0) -> [Word]? {
        dbRecFav = SQLService.sharedInstance.dbRecFav
        do {
            
            let orderTable = recentTable.order(timeInsert.desc)
            let offset = page*limitElementOnPage
            let queryWords = try dbRecFav.prepare(orderTable)
            var index = 0
            var words = [Word]()
            for row in queryWords {
                
                if index >= offset && index < (page+1)*limitElementOnPage{
                    let queryWord = Word(idWord: row[wordId], word: row[word], translate: row[translate], transcription: row[transcription])
                    queryWord.timeAppend = row[timeInsert]
                    words.append(queryWord)
                }else{
                    
                }
                index += 1
            }
            //prepareForRecentHeaders(lastWord: words)
            return words
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
    
    func prepareForRecentHeaders(lastWord: [Word]) -> [WordSearch]{

        let keyWoards = ["Сегодня","Вчера","Давно"]
        var wordResult = [WordSearch]()
        var nowWords = [Word]()
        var yesterdayWords = [Word]()
        var prevYestWords = [Word]()
        let now = Date()
        let noon: Date = Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: now)!
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: noon)!
        let prevYesterday = Calendar.current.date(byAdding: .day, value: -2, to: noon)!
        let yesterdaySeconds = Double(yesterday.timeIntervalSince1970)
        let prevYesterdaySeconds = Double(prevYesterday.timeIntervalSince1970)
        for item in lastWord{
            if item.timeAppend > yesterdaySeconds{
                //print("it now")
                nowWords.append(item)
            }else if item.timeAppend < yesterdaySeconds && item.timeAppend > prevYesterdaySeconds{
                //print("it yeserady")
                yesterdayWords.append(item)
            }else{
                //print("it very old")
                prevYestWords.append(item)
            }
        }
        if !nowWords.isEmpty{
            wordResult.append(WordSearch(words: nowWords, keyS: keyWoards[0]))
        }
        if !yesterdayWords.isEmpty{
            wordResult.append(WordSearch(words: yesterdayWords, keyS: keyWoards[1]))
        }
        if !prevYestWords.isEmpty{
            wordResult.append(WordSearch(words: prevYestWords, keyS: keyWoards[2]))
        }
        
        return wordResult
    }
}
