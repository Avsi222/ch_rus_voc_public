//
//  AntonimsService.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 03/02/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit
import SQLite

class AntonimsService: QueryService {
    
    func findAntonimsForWord(queryWord: String) -> Antonims? {
        database = SQLService.sharedInstance.database
        do {
            let Sqlquery = "SELECT * FROM Antonims WHERE word = \"\(queryWord)\""
            print(Sqlquery)
            let queryWords = try self.database.prepare(Sqlquery)
            let antonim = parseRowAntonims(wordsQuery: queryWords)
            return antonim
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
}
