//
//  WordService.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 03.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit
import SQLite

class WordService: QueryService {
    
    func findWordByWord(query: String) -> Word?{
        
        database = SQLService.sharedInstance.database
        do {
            let sqlquery = "SELECT rowid,word,transcriptionEN FROM fts_ch_word WHERE word MATCH \"\(query)\""
            print(sqlquery)
            let queryWords = try self.database.prepare(sqlquery)
            let words = parseRowDiffLang(wordsQuery: queryWords)
            if words.count > 0 {
                return words[0]
            }else {
                return nil
            }
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
    
    func getKeyRadicalForWord(wordID: Int) -> Word? {
        database = SQLService.sharedInstance.database
        do{
            let sqlQuery = "SELECT rads FROM Cherty WHERE word = \(wordID)"
                //print(sqlQuery)
            let queryWords = try self.database.prepare(sqlQuery)
            for row in queryWords {
                let idWordRow2 = row[0] as! String
                let word = findWordByID(wordID: idWordRow2)
                return word
            }
            return nil
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
    
    func findWordByWordRus(query: String) -> Word?{
        database = SQLService.sharedInstance.database
        do {
            let sqlquery = "SELECT rowid,word FROM fts_ru_word WHERE word MATCH \"\(query)\""
            print(sqlquery)
            let queryWords = try self.database.prepare(sqlquery)
            let words = parseRowRus(wordsQuery: queryWords)
            if words.count > 0 {
                return words[0]
            }else {
                return nil
            }
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
    
    func findWordByID(wordID: String) -> Word?{
        database = SQLService.sharedInstance.database
        do{
            let sqlQuery = "SELECT ChWord.id,fts_ch_word.word ,ChWord.transcription,ChWord.translate FROM ChWord INNER JOIN fts_ch_word on fts_ch_word.ROWID = ChWord.id where id = \(wordID)"
                //print(sqlQuery)
                let queryWords = try self.database.prepare(sqlQuery)
                let word = parseRowForIdSearch(wordsQuery: queryWords)
                return word
            } catch {
                print(error)
                AppDelegate().showError(message: Errors.queryError)
                return nil
            }
    }
    
    func findAndDraw(charForFind: String) -> StrokeOrder?{
        
        print(charForFind)
        strokeDB = SQLService.sharedInstance.strokeDB
        let sql = "select * from t_stroke where character like \"\(charForFind)\""
        do {
            let queryResult = try self.strokeDB.prepare(sql)
            for row in queryResult{
                let character = row[1] as! String
                let stroke = row[3] as! String
                let median = row[4] as! String

                _ = SVGTools.generateStrokePath(with: stroke)
                let word1 = StrokeOrder.init(character: character, strokeString: stroke, medianString: median)
                return word1
            }
        }catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
        return nil
    }
}
