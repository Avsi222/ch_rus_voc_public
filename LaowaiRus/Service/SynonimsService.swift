//
//  SynonimsService.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 04/02/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit
import SQLite

class SynonimsService: QueryService {
    
    func findSynonimsForWord(queryWord: String) -> [Synonims]? {
        database = SQLService.sharedInstance.database
        do {
            let Sqlquery = "SELECT * FROM Synonims WHERE  Synonims like \"%;\(queryWord);%\""
            print(Sqlquery)
            var endSyns = [Synonims]()
            let queryWords = try self.database.prepare(Sqlquery)
            let synonim = parseRowSynonims(wordsQuery: queryWords, queryWord: queryWord)
            if let synonimPar = synonim{
                let crossReference = Dictionary(grouping: synonimPar, by: { $0.type })
                for item in crossReference {
                    //print(item.key)
                    let typeSyn = item.key
                    var stringsItem = [String]()
                    for word in item.value{
                        stringsItem += word.synonimsString
                    }
                    let unique = Array(Set(stringsItem))
                    //print(unique)
                    let synArray = unique
                    let synonimOne = Synonims(type: typeSyn, synonims: synArray)
                    endSyns.append(synonimOne)
                }
            }
            return endSyns
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
}
