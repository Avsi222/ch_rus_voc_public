//
//  SearchService.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 21.01.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit
import SQLite

class SearchService: QueryService {

    //MARK: - Find Word in non fast tables by id
    
    func findCHWordByWord(query: String) -> Word? {
        database = SQLService.sharedInstance.database
        do {
            let sqlQuery = "SELECT ChWord.id,fts_ch_word.word ,ChWord.transcription,ChWord.translate from ChWord INNER JOIN fts_ch_word on fts_ch_word.ROWID = ChWord.id where fts_ch_word.word MATCH \"\(query)\" ORDER BY length(word) LIMIT 1"
            print(sqlQuery)
            let queryWords = try self.database.prepare(sqlQuery)
            let words = parseRowForIdSearch(wordsQuery: queryWords)
            return words
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
    
    func findCHWordIdByWord(query: String) -> Int?{
        database = SQLService.sharedInstance.database
        do {
            let sqlQuery = "SELECT ROWID from fts_ch_word where fts_ch_word.word MATCH \"\(query)\" ORDER BY length(word) LIMIT 1"
            print(sqlQuery)
            let queryWords = try self.database.prepare(sqlQuery)
            for row in queryWords{
                let wordId = row[0] as! Int64
                let intWordId = Int(wordId)
                return intWordId
            }
            return nil
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
    
    public func findCHWordById(id: Int) -> (String,String)? {
        
        database = SQLService.sharedInstance.database
        
        do {
            let sqlquery = "SELECT translate,transcription FROM ChWord WHERE id = \"\(id)\""
            let queryWords = try self.database.prepare(sqlquery)
            for row in queryWords {
                return (row[0] as! String,row[1] as! String)
            }
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
        return nil
    }
    
    public func findRUWordById(id: Int) -> String? {
        
        database = SQLService.sharedInstance.database
        
        do {
            let sqlquery = "SELECT translate FROM RuWord WHERE id = \"\(id)\""
            let queryWords = try self.database.prepare(sqlquery)
            for row in queryWords {
                return row[0] as? String
            }
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
        return nil
    }
    
    
    //MARK: - Find Word in fast tables by word query
    public func findWordsInDiffLanguage(query: String) -> [WordSearch]?{
        database = SQLService.sharedInstance.database
        do {
            //let sqlQuery = "SELECT rowid,word,transcriptionEN FROM fts_ch_word WHERE word MATCH \"*\(query)*\" ORDER BY length(word)"
            let sqlQuery = "SELECT ChWord.id,fts_ch_word.word ,ChWord.transcription,ChWord.translate from ChWord INNER JOIN fts_ch_word on fts_ch_word.ROWID = ChWord.id where fts_ch_word.word MATCH \"*\(query)*\" ORDER BY length(fts_ch_word.word) ASC LIMIT 100"
            print(sqlQuery)
            let queryWords = try self.database.prepare(sqlQuery)
            //let words = parseRowDiffLang(wordsQuery: queryWords)
            let words = TESTparseRowForIdSearch(wordsQuery: queryWords)
            if words.isEmpty{
                decomposeBigWord(query: query)
                return wordForSearch
            }
            let wordForSearch = WordSearch(words: words)
            return [wordForSearch]
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
        
    }
    
    private var wordForSearch = [WordSearch]()
    
    private func decomposeBigWordRussian(query: String){
        wordForSearch.removeAll()
        let wordsArray = query.tokenize()
        for item in wordsArray{
            if item == " "{
                continue
            }
            if let findedWords = specialNonEmptyfindWordsInRULanguage(query: item){
                wordForSearch.append(WordSearch(words: findedWords, keyS: item))
            }
        }
        //wordForSearch.reverse()
    }
    
    private func decomposeBigWord(query: String){
        wordForSearch.removeAll()
        decompose1WordR(query: query)
        wordForSearch.reverse()
    }
    
    func decompose1WordR(query: String){
        if query.isEmpty{
            return
        }
        var queryEdit = query
        let result = decomposeWordR(query: query)
        if result.isEmpty{
            queryEdit.removeLast() //Подумать об этом
        }else{
            let reverse1 = queryEdit.reversed()
            let reverse2 = result.reversed()
            var rev1S = String(reverse1)
            let rev2S = String(reverse2)
            if let range = rev1S.range(of: rev2S) {
                rev1S.removeSubrange(range)
            }
            
            let reverseBack = rev1S.reversed()
            let revBackString = String(reverseBack)
            queryEdit = revBackString
        }
        decompose1WordR(query: queryEdit)
    }
    
    func decomposeWordR(query: String) -> String{
        if query.isEmpty {
            return ""
        }
        var queryEdit = query
        
        if let result = findWordByQuery(wordForFii: queryEdit){
            //slovo est
            return result
        }else{
            //slova net
            queryEdit.removeFirst()
        }
        
        return decomposeWordR(query: queryEdit)
    }
    
    func findWordByQuery(wordForFii: String) -> String?{
        if let wordsFind = specialNonEmptyfindWordsInDiffLanguage(query: wordForFii){
            if wordsFind.isEmpty{
                return nil
            }else{
                let wordS = WordSearch(words: wordsFind, keyS: wordForFii)
                wordForSearch.append(wordS)
                return wordForFii
            }
        }
        return nil
    }
    
    public func specialNonEmptyfindWordsInDiffLanguage(query: String) -> [Word]?{
        database = SQLService.sharedInstance.database
        do {
            let sqlQuery = "SELECT rowid,word,transcriptionEN FROM fts_ch_word WHERE word MATCH \"\(query)\" ORDER BY length(word) LIMIT 1"
            //print(sqlQuery)
            let queryWords = try self.database.prepare(sqlQuery)
            let words = parseRowDiffLang(wordsQuery: queryWords)
            return words
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
    
    public func specialNonEmptyfindWordsInRULanguage(query: String) -> [Word]?{
        database = SQLService.sharedInstance.database
        do {
            let sqlQuery = "SELECT rowid,word FROM fts_ru_word WHERE word MATCH \"\(query.lowercased())\" ORDER by length(word) LIMIT 1"
            print(sqlQuery)
            let queryWords = try self.database.prepare(sqlQuery)
            let words = parseRowRus(wordsQuery: queryWords)
            return words
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
    
    public func findWordInRu(query: String) -> [WordSearch]? {
        database = SQLService.sharedInstance.database
        do {
            let sqlquery = "SELECT rowid,word FROM fts_ru_word WHERE word MATCH \"\(query.lowercased())*\" ORDER BY length(word)"
            print(sqlquery)
            let queryWords = try self.database.prepare(sqlquery)
            let words = parseRowRus(wordsQuery: queryWords)
            if words.isEmpty{
                decomposeBigWordRussian(query: query)
                return wordForSearch
            }
            let wordsForSearch = WordSearch(words: words)
            return [wordsForSearch]
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }

    //MARK: - Find Word in fast tables by transcription query
    func findWordByTranscription(query: String) -> [WordSearch]? {
        database = SQLService.sharedInstance.database
        do {
            
            let sqlQuery = "SELECT ChWord.id,fts_ch_word.word ,ChWord.transcription,ChWord.translate from ChWord INNER JOIN fts_ch_word on fts_ch_word.ROWID = ChWord.id where fts_ch_word.transcriptionEN MATCH \"\(query)\" ORDER BY ChWord.weight DESC LIMIT 100"
            //let sqlQuery = "SELECT rowid,word,transcriptionEN FROM fts_ch_word WHERE transcriptionEN MATCH \"\(query)*\" ORDER BY length(transcriptionEN) ASC"
            print(sqlQuery)
            let queryWords = try self.database.prepare(sqlQuery)
            let words = TESTparseRowForIdSearch(wordsQuery: queryWords)
            let wordForSearch = WordSearch(words: words)
            return [wordForSearch]
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
    // Use alternative search if the first one failed
    func findWordByTranscriptionAlt(query: String) -> [WordSearch]? {
        database = SQLService.sharedInstance.database
        do {
            
            let sqlQuery = "SELECT ChWord.id,fts_ch_word.word ,ChWord.transcription,ChWord.translate from ChWord INNER JOIN fts_ch_word on fts_ch_word.ROWID = ChWord.id where fts_ch_word.transcriptionEN MATCH \"\(query)*\" ORDER BY ChWord.weight DESC LIMIT 100"
            //let sqlQuery = "SELECT rowid,word,transcriptionEN FROM fts_ch_word WHERE transcriptionEN MATCH \"\(query)*\" ORDER BY length(transcriptionEN) ASC"
            print(sqlQuery)
            let queryWords = try self.database.prepare(sqlQuery)
            let words = TESTparseRowForIdSearch(wordsQuery: queryWords)
            let wordForSearch = WordSearch(words: words)
            return [wordForSearch]
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }
        
    //MARK: - Find Example for word by word query
    func findExampleForWord(queryWord: String) -> [Example]? {
        database = SQLService.sharedInstance.database
        do {
            let sqlquery = "SELECT * FROM Example WHERE Example like \"%\(queryWord)%\""
            print(sqlquery)
            let queryWords = try self.database.prepare(sqlquery)
            let examples = parseRowExamples(wordsQuery: queryWords)
            return examples
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
            return nil
        }
    }

}
