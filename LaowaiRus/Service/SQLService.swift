//
//  DatabaseService.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 21.01.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import SQLite
import UIKit

class SQLService{
    
    static let sharedInstance = SQLService()
    
    public var database: Connection!
    public var dbRecFav: Connection!
    public var strokeDB: Connection!
    
    private var baseName = "bkrs_3.db"
    private var strokeBaseName = "strokes.db"
    
    public let pinyinFavTable = Table("PinyinFav")
    
    public let recentTable = Table("Recent122")
    
    public let oldRecentTable = Table("Recent2")
    
    public let favoriteTable = Table("Favorite")
    
    public let favoriteListTable = Table("FavoriteList")
    
    public let id = Expression<Int>("id")
    
    public let word = Expression<String>("word")
    
    public let translate = Expression<String>("translate")
    
    public let transcription = Expression<String>("transcription")
    
    public let wordId = Expression<Int>("wordId")
    
    public let listId = Expression<Int>("listId")
    
    public let listName = Expression<String>("listName")
    
    public let timeInsert = Expression<Double>("dateInsert")
    
    public func initTable(){
        do {
            _ = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(baseName)
            if let path = Bundle.main.path(forResource: baseName, ofType: nil){
                let url = URL(fileURLWithPath: path)
                let database = try Connection(url.path, readonly: false)
                self.database = database
                SQLService.sharedInstance.database = database
                initStrokeTable()
                createBaseForRecent()
                createBaseForPinyinFav()
                createBaseForFavorite()
                createBaseForFavoriteLists()
            }else{
                AppDelegate().showError(message: Errors.databaseError)
            }
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.databaseError)
        }
    }
       
    public func initStrokeTable(){
        do {
            _ = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(strokeBaseName)
            if let path = Bundle.main.path(forResource: strokeBaseName, ofType: nil){
               let url = URL(fileURLWithPath: path)
               let database = try Connection(url.path, readonly: false)
               self.strokeDB = database
               SQLService.sharedInstance.strokeDB = database
            }else{
                AppDelegate().showError(message: Errors.databaseError)
            }
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.databaseError)
        }
    }
    
    private func createBaseForRecent(){
       do{
        let path = NSSearchPathForDirectoriesInDomains(
            .documentDirectory, .userDomainMask, true
        ).first!
        print(path)
        let dbRecFav = try Connection("\(path)/db.sqlite3")
        self.dbRecFav = dbRecFav
        SQLService.sharedInstance.dbRecFav = dbRecFav
            do {
                _ = try dbRecFav.scalar(recentTable.exists)
                //exists
            } catch {
                //doesn't
                try dbRecFav.run(recentTable.create { t in
                t.column(wordId, primaryKey: .default)
                    t.column(word)
                    t.column(translate)
                    t.column(transcription)
                    t.column(timeInsert)
                    //t.column(wordId)
                })
            }
        
        checkOldRecents()
        } catch {
            print(error)
            AppDelegate().showError(message: Errors.queryError)
        }
    }
    
    private func checkOldRecents(){
        do {
            _ = try dbRecFav.scalar(oldRecentTable.exists)
            //exists
            migrateOldRecentsToNew()
        } catch {
            return
        }
    }
    
    private func migrateOldRecentsToNew(){
        do {
            let recents = try self.dbRecFav.prepare(oldRecentTable)
            var words = [Word]()
            for row in recents {
                let wordRow = row[word]
                if let wordFinded = SearchService().findCHWordByWord(query: wordRow){
                    words.append(wordFinded)
                }
            }
            let resultDrop = oldRecentTable.drop()
            try self.dbRecFav.run(resultDrop)
            print(resultDrop)
            for wordF in words{
                RecentService().saveToLast(word: wordF)
            }
        }catch {
                
        }
    }
       
    private func createBaseForFavorite(){
       do{
           do {
               _ = try dbRecFav.scalar(favoriteTable.exists)
               //exists
           } catch {
               //doesn't
               try dbRecFav.run(favoriteTable.create { t in
                   t.column(id, primaryKey: .autoincrement)
                   t.column(word)
                   t.column(translate)
                   t.column(transcription)
                   t.column(wordId)
                   t.column(listId)
               })
           }
        
       } catch {
           print(error)
           AppDelegate().showError(message: Errors.queryError)
       }
    }
    
    private func createBaseForPinyinFav(){
       do{
           do {
               _ = try dbRecFav.scalar(pinyinFavTable.exists)
               //exists
           } catch {
               //doesn't
               try dbRecFav.run(pinyinFavTable.create { t in
                   t.column(id, primaryKey: .autoincrement)
                   t.column(word)
                t.column(timeInsert)
               })
           }
        
       } catch {
           print(error)
           AppDelegate().showError(message: Errors.queryError)
       }
    }
       
    private func createBaseForFavoriteLists(){
       do{
           do {
               _ = try dbRecFav.scalar(favoriteListTable.exists)
           } catch {
               try dbRecFav.run(favoriteListTable.create { t in
                   t.column(listId, primaryKey: .autoincrement)
                   t.column(listName)
               })
           }
       } catch {
           print(error)
           AppDelegate().showError(message: Errors.queryError)
       }
    }
      
}
