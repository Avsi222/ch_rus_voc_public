//
//  Extension+UIViewController.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 03/02/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func openWord(word: Word) {
        let wordVC = UIStoryboard.init(name: "Word", bundle: Bundle.main).instantiateViewController(withIdentifier: "WordViewController")
            as? WordViewController
        
        word.findExamples()
        if !word.isRus {
            word.findAntonims()
            word.findSynonims()
            word.findCountings()
            word.findRadicals()
            word.findStrokes()
        }
        
        RecentService().saveToLast(word: word)
        wordVC?.currentWord = word
        wordVC?.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(wordVC!, animated: true)
        
    }
    
    func openPinyin(text: String) {
        let piniynVC = UIStoryboard.init(name: "Pinyin", bundle: Bundle.main).instantiateViewController(withIdentifier: "PinyinViewController")
            as? PinyinViewController
        piniynVC?.text = text
        piniynVC?.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(piniynVC!, animated: true)
    }
    
    func openSearch(text: String){
        if !text.isEmpty{
            let vc = UIStoryboard.init(name: "Search", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
            vc?.initedQuery = text
            vc?.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    func openUrlInSafari(urlStr: String){
        let encodedUrl = urlStr.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        guard let url = URL(string: encodedUrl) else {
            print("url is bad")
            return
        }
        UIApplication.shared.open(url)
    }
}
