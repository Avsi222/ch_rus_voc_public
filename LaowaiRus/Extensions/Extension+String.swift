//
//  Extension+String.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 29/01/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit
import NaturalLanguage

extension String {
    func detectedLangauge() -> String? {
        let recognizer = NLLanguageRecognizer()
        recognizer.processString(self as String)
        guard let languageCode = recognizer.dominantLanguage?.rawValue else { return nil }
        return languageCode
    }
    func indexOf(target: String) -> Int? {
        let range = (self as NSString).range(of: target)
        guard range.toRange() != nil else {
            return nil
        }
        return range.location
    }
    func tokenize(options: Tokenizer.TokenizeOption = .default) -> [String] {
              return Tokenizer.default.tokenize(text: self, options: options)
          }
          static func tokenize(string sentence: String, options: Tokenizer.TokenizeOption = .default) -> [String] {
              return Tokenizer.default.tokenize(text: sentence, options: options)
          }
 
    func widthOfString(usingFont font: UIFont) -> CGFloat {
          let fontAttributes = [NSAttributedString.Key.font: font]
          let size = self.size(withAttributes: fontAttributes)
          return size.width
    }

    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }

    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
    
    
}
extension StringProtocol {
    var firstUppercased: String { prefix(1).uppercased() + dropFirst() }
    var firstCapitalized: String { prefix(1).capitalized + dropFirst() }
}
