//
//  Extension+SearchBar.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 11.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

extension UISearchBar {

    var textField: UITextField? {
        let subViews = subviews.flatMap { $0.subviews }
        return (subViews.filter { $0 is UITextField }).first as? UITextField
    }
}
