//
//  DateExtension.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 05/02/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import Foundation


class TimeChecker{
    func getNow(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm:ss.SSSS"
        print(dateFormatter.string(from: Date()))
    }
}
