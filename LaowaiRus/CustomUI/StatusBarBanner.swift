//
//  StatusBarBanner.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 03.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import NotificationBannerSwift
import UIKit

class StatusBarBanner{

    let notifDelay = 1.5
    
    private func showBanner(banner: StatusBarNotificationBanner){
        banner.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + notifDelay) {
            banner.dismiss()
        }
    }

    public func showErrorWithTitle(title: String) {
        
        let banner = StatusBarNotificationBanner(title:   NSLocalizedString(title, comment: "Произошло что-то очень страшное"), style: .danger)
        showBanner(banner: banner)
    }
    
    public func successAddToList(){

        let banner = StatusBarNotificationBanner(title:   NSLocalizedString("Добавлено в список", comment: "Subtitle for favorite popup"), style: .success)
        showBanner(banner: banner)
    }
    
    public func successPinyinAddToList(){

        let banner = StatusBarNotificationBanner(title:   NSLocalizedString("Добавлено в тексты", comment: "Subtitle for favorite popup"), style: .success)
        showBanner(banner: banner)
    }
    
    public func alreadyListCreated(){

        let banner = StatusBarNotificationBanner(title:   NSLocalizedString("Такой список уже есть", comment: "Subtitle for favorite popup"), style: .danger)
        showBanner(banner: banner)
    }
    
    public func alreadyInList(){
        let banner = StatusBarNotificationBanner(title:   NSLocalizedString("Уже есть в этом списке", comment: "Subtitle for favorite popup"), style: .danger)
        showBanner(banner: banner)
    }
    
    public func alreadyInPinyinList(){
        let banner = StatusBarNotificationBanner(title:   NSLocalizedString("Уже есть в текстах", comment: "Subtitle for favorite popup"), style: .danger)
        showBanner(banner: banner)
    }
    
    public func emptyListName(){
        let banner = StatusBarNotificationBanner(title:   NSLocalizedString("Нельзя создать список без имени", comment: "UI alert controller title"), style: .danger)
        showBanner(banner: banner)
    }
    
    public func successCopiedInBuffer(){
        let banner = StatusBarNotificationBanner(title:   NSLocalizedString("Скопировано в буфер обмена", comment: "UI alert controller title"), style: .success)
        showBanner(banner: banner)
    }
}
