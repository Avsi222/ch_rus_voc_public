//
//  SpinnerLoad.swift
//  DoichVocalbury
//
//  Created by Mitya Polyanskij on 19.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//
import UIKit

class SpinnerViewController: UIViewController {
    var spinner = UIActivityIndicatorView(style: .whiteLarge)

    override func loadView() {
        view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)

        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        view.addSubview(spinner)

        spinner.centerXAnchor.constraint(equalTo: super.view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: super.view.centerYAnchor).isActive = true
    }
}
