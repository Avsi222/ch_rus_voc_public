//
//  EmptyTableViewCell.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 11.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class EmptyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var helperImageView: UIImageView!
    @IBOutlet weak var helperNameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initCell(name: String, image: UIImage){
        helperImageView.image = image
        helperNameLabel.text = name
    }
}
