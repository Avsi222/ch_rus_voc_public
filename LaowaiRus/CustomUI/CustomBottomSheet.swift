//
//  CustomBottomSheet.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 03.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit
import NotificationBannerSwift

class CustomBottomSheet: UIAlertController {
    
    var favoriteLists = [FavoriteList]()
    var currentWord: Word! = nil
    var parentVC: WordViewController! = nil
    
    
    func initController(vc: WordViewController, word: Word) {
        currentWord = word
        parentVC = vc
        
        if let favList = FavoriteService().getListFav() {
            favoriteLists = favList
        }
        
        initDefaultAction()
        initFavListsAction()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func initFavListsAction(){
        for list in favoriteLists{

            let action = UIAlertAction(title: list.name, style: .default) { (action) in
                if FavoriteService().addWordToFavoriteList(word: self.currentWord, list: list.id){
                    StatusBarBanner().successAddToList()
                }else{
                    StatusBarBanner().alreadyInList()
                }
            }
            self.addAction(action)
        }
    }
    
    func initDefaultAction(){
        let newListAction = UIAlertAction(title: "Новый список", style: .default) { (action) in
            self.askNameList { (nameList) in
                if let nameList = nameList{
                    if self.favoriteLists.contains(where: {$0.name == nameList}) {
                        StatusBarBanner().alreadyListCreated()
                    } else {
                        FavoriteService().addNewFavoriteList(name: nameList, id: self.favoriteLists.count)
                        self.parentVC.showAlertAddToFav()
                    }
                }
            }
        }
        self.addAction(newListAction)
        
        let cancelAction = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
        self.addAction(cancelAction)
    }
    
    func askNameList(completition: @escaping (String?) -> ()){
        var textField2 = UITextField()
        let addNewListalert = UIAlertController(title: "Добавить новый список", message: nil, preferredStyle: .alert)
        addNewListalert.addTextField { (textField) in
            textField.autocapitalizationType = .sentences
            textField2 = textField
        }
        let action = UIAlertAction(title: "Ок", style: .default) { (action) in
             if textField2.text!.isEmpty{
                StatusBarBanner().emptyListName()
                completition(nil)
             }else{
                completition(textField2.text)
             }
        }
        addNewListalert.addAction(action)
        
        parentVC.present(addNewListalert, animated: true)
        
    }
}
