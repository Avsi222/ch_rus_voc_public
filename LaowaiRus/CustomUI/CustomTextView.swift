//
//  UITextView custom.swift
//  BKRS
//
//  Created by Mitya Polyanskij on 13.01.2020.
//  Copyright © 2020 Арсений Дорогин. All rights reserved.
//

import UIKit
import AVFoundation

class CustomTextView: UITextView {
    
    var parentVC: UIViewController!
    
    let menuItem1: UIMenuItem = UIMenuItem(title: "Пиньинь", action: #selector(onMenu1(sender:)))
    let menuItem2: UIMenuItem = UIMenuItem(title: "Поиск", action: #selector(onMenu2(sender:)))
    let menuItem3: UIMenuItem = UIMenuItem(title: "Произнести", action: #selector(onMenu3(sender:)))
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let menuController: UIMenuController = UIMenuController.shared
        
        menuController.setMenuVisible(true, animated: true)
        
        menuController.arrowDirection = UIMenuController.ArrowDirection.default
        
        // Store MenuItem in array.
        let myMenuItems: [UIMenuItem] = [menuItem1,menuItem2,menuItem3]
        
        // Added MenuItem to MenuController.
        menuController.menuItems = myMenuItems
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any!) -> Bool {
        if [#selector(copy(_:)), #selector(onMenu1(sender:)), #selector(onMenu2(sender:)), #selector(onMenu3(sender:))].contains(action) {
            return true
        }
        else{
            return false
        }
    }
    
    @objc internal func onMenu1(sender: UIMenuItem) {
        print("onMenu1")
        let range = self.selectedTextRange!
        let data = self.text(in: range)!
        print(data)
        parentVC.openPinyin(text: data)
    }
    
    @objc internal func onMenu2(sender: UIMenuItem) {
        print("onMenu2")
        let range = self.selectedTextRange!
        let data = self.text(in: range)!
        print(data)
        parentVC.openSearch(text: data)
    }
    
    @objc internal func onMenu3(sender: UIMenuItem) {
        print("onMenu3")
        let speechSynthesizer = AVSpeechSynthesizer()
        let range = self.selectedTextRange!
        let data = self.text(in: range)!
        let speechUtterance = AVSpeechUtterance(string: data)
                speechUtterance.voice = AVSpeechSynthesisVoice(language: "zh-CN")
                do {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
                }
                catch {
                   // catch error
                }
            speechSynthesizer.speak(speechUtterance)
    }
}

