//
//  WordLabel.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 06.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class WordLabel: UILabel {
    
    func addText(word: String, transcription: String){
        let attrTextWordAndTrans = NSMutableAttributedString(string: word + " " + transcription)
        let range1 = NSRange(location: 0, length: word.count+1)
        attrTextWordAndTrans.addAttribute(.font, value: UIFont.wordCellLabelFont, range: range1)
        if !transcription.isEmpty{
            let range2 = NSRange(location: word.count + 1, length: transcription.count)
            attrTextWordAndTrans.addAttribute(.font, value: UIFont.transcriptionCellLabelFont, range: range2)
        }
        self.attributedText = attrTextWordAndTrans
    }

}
