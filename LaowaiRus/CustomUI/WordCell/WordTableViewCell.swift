//
//  WordTableViewCell.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 04/02/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

class WordTableViewCell: UITableViewCell {

    @IBOutlet weak var wordLabel: WordLabel!
    @IBOutlet weak var translateLabel: UILabel!
    
    func initCell(word: Word){
        wordLabel.addText(word: word.word , transcription: word.transcription)
        translateLabel.attributedText = word.combination
        
    }
    
}
//鼓舞
