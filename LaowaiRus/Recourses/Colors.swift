//
//  Colors.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 21.01.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

extension UIColor {
    static let launchColor = UIColor(named: "launchColor") ?? UIColor.black
    //
    static let systemGrayBgColor = UIColor(named: "navigationBarColor") ?? UIColor.black
    //
    static let systemYellowTintColor = UIColor(named: "buttonColor") ?? UIColor.black
    //
    static let mainWordBGColor = UIColor(named: "wordBarColor") ?? UIColor.black
    //
    static let wordTextCellColor = UIColor(named: "cellTitleColor") ?? UIColor.black
    //
    static let navBarTintColor = systemYellowTintColor
    //
    static let translateTextCellColor = UIColor(named: "textViewTextColor") ?? UIColor.black
    //
    static let navBarBackgroundColor = systemGrayBgColor
    //
    static let tabBarBackgroundColor = systemGrayBgColor
    //
    static let tabBarTintColor = systemYellowTintColor
    //
    static let backgroundColor = systemGrayBgColor
    //
    static let barButtonsTintColor = systemYellowTintColor
    //
    static let buttonsTintColor = systemYellowTintColor
    //
    static let searchBarBackgroundColor = UIColor(named: "searchBarColor") ?? UIColor.black
    //
    static let searchBarTintColor = UIColor.black
    //
    static let wordWordColor = systemYellowTintColor
    static let transcriptWordColor = UIColor.white
    //
    static let defaultTranslateWordColor = UIColor(named: "textViewTextColor") ?? UIColor.black
    static let boldTranslateWordColor = UIColor(named: "textViewTextColor") ?? UIColor.black
    static let exampleTranslateWordColor = UIColor.gray
    static let italicTranslateWordColor = UIColor(named: "textViewTextColor") ?? UIColor.black
    static let linkTranslateWordColor = UIColor.blue
    static let colorTranslateWordColor = UIColor.red
    //
    static let favoriteListBGColor = UIColor(named: "backgroundViewColor") ?? UIColor.black
    static let favoriteListNameLabelTintColor = systemYellowTintColor
    //
    static let pinyinEditorTextViewTextColor = UIColor(named: "textViewTextColor") ?? UIColor.black
    //
    static let strokeColor = UIColor(named: "textViewTextColor") ?? UIColor.black
}
