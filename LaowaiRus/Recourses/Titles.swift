//
//  Strings.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 03.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import Foundation

class Titles {
    static let historyListName = "История поиска"
    static let piniynListName = "Тексты"
    //
    static let rowActionDelete = "Удалить"
    //
    static let rowActionChange = "Переим."
    //
    static let titleAlertChangeNameList = "Изменение"
    static let messageAlertChangeNameList = "Изменение"
    static let saveAlertChangeNameList = "Сохранить"
    //
    static let alertCancelAction = "Отмена"
    //
    static let segmentAntonymsTitle = "Антонимы"
    static let segmentSynonymsTitle = "Синонимы"
    static let segmentTranslateTitle = "Перевод"
    static let segmentExamplesTitle = "Примеры"
    static let segmentStrokesTitle = "Иероглиф"
}
