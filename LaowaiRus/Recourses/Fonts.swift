//
//  Fonts.swift
//  DoichVocalbury
//
//  Created by Andrew Sokolov on 03.02.2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

extension UIFont {
    //
    static let wordLabelFont = UIFont.systemFont(ofSize: 30)
    static let transcriptionLabelFont = UIFont(name: "SFProText-Bold", size: 17.0) ?? UIFont.boldSystemFont(ofSize: 17)
    static let exampleFont = UIFont(name: "SFProText-Regular", size: 19.0) ?? UIFont.systemFont(ofSize: 19)
    static let countingButtonFont = UIFont.boldSystemFont(ofSize: 24)
    static let segmentTitle = UIFont.systemFont(ofSize: 13)
    //
    static let defaultTranslateFont = UIFont(name: "SFProText-Regular", size: 19.0) ?? UIFont.systemFont(ofSize: 19)
    static let boldTranslateFont = UIFont(name: "SFProText-Bold", size: 19.0) ?? UIFont.boldSystemFont(ofSize: 19)
    static let exampleTranslateFont = UIFont(name: "SFProText-Regular", size: 17.0) ?? UIFont.systemFont(ofSize: 17)
    static let italicTranslateFont = UIFont(name: "SFProText-RegularItalic", size: 19.0) ?? UIFont.systemFont(ofSize: 19)
    static let linkTranslateFont = UIFont(name: "SFProText-Regular", size: 19.0) ?? UIFont.systemFont(ofSize: 19)
    static let colorTranslateFont = UIFont(name: "SFProText-RegularItalic", size: 19.0) ?? UIFont.systemFont(ofSize: 19)
    //
    static let wordCellLabelFont = UIFont.systemFont(ofSize: 23)
    static let transcriptionCellLabelFont = UIFont.boldSystemFont(ofSize: 17)
    //
    static let favoriteListCellLabelFont = UIFont.systemFont(ofSize: 23)
    //
    static let pinyinTextLabelFont = UIFont(name: "SFProText-Regular", size: 24.0) ?? UIFont.systemFont(ofSize: 24)
    static let pinyinCharacterLabelFont = UIFont(name: "SFProText-Regular", size: 13.0) ?? UIFont.systemFont(ofSize: 13)
    //
    static let pinyinEditorTextViewFont = UIFont(name: "SFProText-Regular", size: 24.0) ?? UIFont.systemFont(ofSize: 24)
}
