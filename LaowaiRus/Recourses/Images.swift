//
//  Images.swift
//  DoichVocalbury
//
//  Created by Арсений Дорогин on 03/02/2020.
//  Copyright © 2020 AVSI. All rights reserved.
//

import UIKit

extension UIImage {
    //
    static let shareButton = UIImage(named: "share")
    static let speakButton = UIImage(named: "sound")
    static let fuckup = UIImage(named: "fuckup")
    static let searchButton = UIImage(named: "Search")
    //
    static let addToFavButton = UIImage(named: "+")
}
